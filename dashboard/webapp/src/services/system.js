import { logout as userLogout } from 'services/user'

export async function logout() {
  await userLogout()
  window.location.reload()
}