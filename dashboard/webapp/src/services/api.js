export const baseURL = 'http://localhost:8000/api'

const getToken = () => window.localStorage.getItem('mysli-api-auth-token');

export default async function api(url, other={}) {
  let headers = other.headers ? other.headers : {}
  let token = getToken()
  if (token) {
    headers = {
      Authorization: `Basic ${token}`,
      credentials: 'include',
      ...headers
    }
  }

  const response = await fetch(`${baseURL}/${url}`, {
    method: 'GET',
    cache: 'no-cache',
    mode: 'cors',
    ...other,
    headers
  })

  const data = await response.json();
  return { response, data }
}

export async function post(url, data) {
  return api(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
}
