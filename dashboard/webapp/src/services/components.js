// dynamically loaded components, for usage in menus etc...
import ApiUsers from 'pages/Users'
import ApiSettings from 'pages/Settings'
import ApiPages from 'pages/Pages'
import ApiPagesEditor from 'pages/Pages/Editor'
import ApiNavigator from 'pages/Navigator'

let components = {}
components = {
  'api.navigator': {component: ApiNavigator, target: 'navigator'},
  'api.users': {component: ApiUsers, target: 'explorer'},
  'api.settings': {component: ApiSettings, target: 'explorer'},
  'api.pages': {component: ApiPages, target: 'explorer'},
  'api.pages.editor': {component: ApiPagesEditor, target: 'contents'}
}
window.mysliComponents = components
export default components