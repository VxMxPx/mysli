import { writable } from 'svelte/store'
import api, { post } from 'api'

export const user = writable({})

function update(userData) {
  user.update(() => userData)
  if (userData.session) {
    window.localStorage.setItem('mysli-api-auth-token', userData.session.id)
  } else {
    window.localStorage.removeItem('mysli-api-auth-token')
  }
}

export async function login(username, password) {
  const r = await post('auth', {username, password})
  r.response.status === 200 && update(r.data)
  return r
}

export async function logout() {
  const r = await api('auth', {method:'DELETE'})
  r.response.status === 200 && update({})
  return r
}

export async function auth() {
  const r = await api('auth')
  r.response.status === 200 && update(r.data)
  return r
}