import { writable } from 'svelte/store'
import components from './components'

export function openPathFromHash() {
  const path = window.location.hash.substr(1)
  openPath(path)
}

export function openPath(path, args={}) {
  if (typeof components[path] !== 'object') return
  const component = components[path]
  panels.update(panels => {
    if (typeof panels[component.target] === 'function' &&
      panels[component.target].component.name === component.component.name) {
      panels[component.target] = null
    } else {
      panels[component.target] = { ...component, args }
    }
    if (component.target === 'explorer') {
      window.location.hash = path
    }
    return panels
  })
}

export const panels = writable({
  navigator: null,
  explorer: null,
  contents: null,
  sidebar: null
})
