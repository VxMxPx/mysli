// global resources
import '@fortawesome/fontawesome-free/js/all.js'

// compoents
import App from './App.svelte'

var app = new App({
  target: document.body
})

export default app