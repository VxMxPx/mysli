<?php

/**
 * Parse Markdown string.
 * Note: There's \markdown\markdown available,
 * for static access to an instance of this class.
 */
namespace markdown;

use \markdown\lines;
use \sys\{str,log};

class parser
{
    /**
     * How lines should be processed, this allows plugging costume processors.
     */
    protected $processors = [
      'markdown.module.code_backtick' => null,
      'markdown.module.html'          => null,
      'markdown.module.blockquote'    => null,
      'markdown.module.listf'         => null,
      'markdown.module.code_indent'   => null,
      'markdown.module.entity'        => null,
      'markdown.module.header'        => null,
      'markdown.module.rule'          => null,
      'markdown.module.container'     => null,
      'markdown.module.paragraph'     => null,
      'markdown.module.inline'        => null,
      'markdown.module.link'          => null,
      'markdown.module.url'           => null,
      'markdown.module.typography'    => null,
      'markdown.module.abbreviation'  => null,
      'markdown.module.footnote'      => null,
      'markdown.module.unseal'        => null,
    ];

    /**
     * Input markdown broken to lines.
     */
    protected $markdown;

    /**
     * Markdown source in lines.
     * --
     * @var lines
     */
    protected $lines;

    /**
     * Construct parser.
     * --
     * @param string $markdown
     */
    function __construct($markdown)
    {
        log::info('I shall process: `'.mb_substr($markdown, 0, 140).'...`.', __CLASS__);
        $this->markdown = explode("\n", str::to_unix_line_endings($markdown));
        $this->lines = new lines($this->markdown);
    }

    /**
     * Return lines.
     * --
     * @return lines
     */
    function get_lines()
    {
        return $this->lines;
    }

    /**
     * Return all processor(s).
     * --
     * @return lines
     */
    function get_processors()
    {
        return $this->processors;
    }

    /**
     * Return particular processor by ID. This will instantiate object if not there.
     * --
     * @param string $id
     * --
     * @return object
     */
    function get_processor($id)
    {
        if (!array_key_exists($id, $this->processors))
          err('invalid_processor_id', $id);

        if (!$this->processors[$id])
        {
            $class = str_replace('.', '\\', $id);
            $this->processors[$id] = new $class($this->lines);
        }

        return $this->processors[$id];
    }

    /**
     * Run process and return output.
     * --
     * @return markdown\output
     */
    function process()
    {
        $at = 0;

        $this->lines->reset();

        // Blocks
        foreach ($this->processors as $processor => $instance)
        {
            if (!$instance)
            {
                $instance = $this->get_processor($processor);
            }

            $r = $instance->process($at);

            // Skip forward
            if (is_numeric($r)) $at = $r;

            // Break the loop
            if ($r === false) return;
        }

        return $this->lines;
    }
}
