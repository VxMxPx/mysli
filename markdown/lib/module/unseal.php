<?php

/**
 * Unseal previously sealed lines.
 */
namespace markdown\module; class unseal extends module
{
  /**
   * --
   * @param integer $at
   */
  function process($at)
  {
    while ($this->lines->has($at))
    {
      $this->lines->set($at, $this->unseal($at));
      $at++;
    }
  }
}
