<?php

#: Before
use \markdown\markdown;

#: Test Basic
$markdown = <<<MARKDOWN
---

-----

  ----

___

_____

***********************
MARKDOWN;

return assert::equals(markdown::process($markdown),
'<hr/>
<hr/>
<hr/>
<hr/>
<hr/>
<hr/>');
