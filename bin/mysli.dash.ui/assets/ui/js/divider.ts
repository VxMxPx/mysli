/// <reference path="widget.ts" />

module mysli.js.ui
{
    export class Divider extends Widget
    {
        protected static template: string = '<hr class="ui-divider" />';
    }
}
