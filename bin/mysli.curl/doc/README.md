# cURL

A simple cURL interface.

## Table of Contents

- [Examples](examples.md)
- [Configuration](configuration.md)
- [API](api.md)
- [License](license.md)
