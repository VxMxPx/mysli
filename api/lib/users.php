<?php namespace api;

use \sys\{str, ini};
use \sys\fs\{file, dir};

class users
{
  static $users = [];

  static function id_from_email(string $email): string
  {
    $email_md5 = substr(md5($email), 0, 6);
    $email_clean = str_replace(['@', '.'], ['_at_', '__'], $email);
    $email_clean = str::clean($email_clean, 'slug');
    return $email_clean.'__'.$email_md5;
  }

  static function password_hash(string $password): string
  {
    return password_hash($password, PASSWORD_BCRYPT);
  }

  static function ids(): array
  {
    if (!count(self::$users))
    {
      $udir = datpath('etc/users');
      $users = scandir($udir);
      foreach($users as $user) {
        if (file_exists(ds($udir, $user, 'user.ini'))) {
          self::$users[] = $user;
        }
      }
    }
    return self::$users;
  }

  static function create(string $email, string $password)
  {
    $id = self::id_from_email($email);
    if (self::exists($email)) err('user_already_exists', $id);

    $record = [
      'id' => $id,
      'email' => $email,
      'password' => self::password_hash($password),
      'groups' => [],
      'language' => 'en',
      'created_on' => date('c'),
      'updated_on' => date('c')
    ];
    # write user data
    $path = datpath('etc/users', $id);
    (dir::create($path) && ini::encode_file(ds($path, 'user.ini'), $record)) or err('failed_to_create_user', $path);

    # load user record
    $user = self::get($email);
    $user->log('api.users.create', user::log_ok, $id.' '.$email);

    return $user;
  }

  static function exists(string $email): bool
  {
    $id = self::id_from_email($email);
    return self::exists_by_id($id);
  }

  static function exists_by_id(string $id): bool
  {
    return file_exists(datpath('etc/users/', $id, 'user.ini'));
  }

  static function get(string $email)
  {
    if (!self::exists($email)) return null;
    $id = self::id_from_email($email);
    return self::get_by_id($id);
  }

  static function get_by_id(string $id)
   {
    if (!self::exists_by_id($id)) return null;
    $path = datpath('etc/users', $id);
    $data = ini::decode_file(ds($path, 'user.ini'));
    return new user($data);
  }

  static function log_add(string $id, string $line): bool
  {
    $path = datpath('etc/users', $id);
    $logfile = ds($path, 'logs');
    return !! file::write($logfile, $line."\n", file::append);
  }

  static function remove($email): bool
  {
    if (!is_object($email)) {
      if (!self::exists($email)) return null;
      $id = self::id_from_email($email);
    } else {
      $user = $email;
      $id = $user->id;
    }
    $path = datpath('etc/users', $id);
    return dir::remove($path);
  }

  static function write(\api\user $user): bool
  {
    $path = datpath('etc/users', $user->id, 'user.ini');
    $data = $user->data;
    return ini::encode_file($path, $data);
  }
}