<?php namespace api;

use \sys\{str, ini};

class session
{
  static $expire = 60*60*24*12;

  # get sessions filename from session id / user id
  static function filename_by_id(string $id)
  {
    if (strpos($id, '.')) {
      list($userid, $session) = explode('.', $id);
    } else {
      $userid = $id;
    }

    if (!$id) return logw("invalid_filename_id:${id}", __CLASS__);

    $filename = datpath('etc/users', $userid, 'sessions.ini');

    if (!file_exists($filename)) return logw("no_such_file:${filename}", __CLASS__);

    return $filename;
  }

  # get all sessions by session ID or user ID
  static function get_sessions(string $id)
  {
    $filename = self::filename_by_id($id);
    if (!$filename) return false;
    $sessions = ini::decode_file($filename);
    if (!$sessions || !is_array($sessions)) return [];
    else return $sessions;
  }

  # get session array by session id
  static function get(string $id)
  {
    $sessions = self::get_sessions($id);

    if (!$sessions || !isset($sessions[$id]))
      return logi("no_session_found:${id}", __CLASS__) && false;
    else $session = $sessions[$id];

    if ($session['expire'] <= time()) {
      self::delete($id);
      return logi("session_was_expired:${id}", __CLASS__) && false;
    }

    # append user id
    list($userid, $_) = explode('.', $id);
    $session['userid'] = $userid;

    return $session;
  }

  # generate a new session
  static function generate(string $userid, int $expire=-1): array
  {
    # create a token and session IDs
    $id = $userid.'.'.sha1(uniqid('', true).str::random(10).microtime(true));
    $expire = time() + ($expire > -1 ? $expire : self::$expire);

    $geo = function_exists('geoip_record_by_name')
      ? geoip_record_by_name('46.123.243.155')
      : null;

    $session = [
      'id' => $id,
      'expire' => $expire,
      'agent' => req()->agent(),
      'ip' => req()->ip(),
      'location' => $geo && isset($geo['country_name']) ? $geo['country_name'] : ''
    ];

    return $session;
  }

  # write session to file
  static function write(array $session): bool
  {
    $id = $session['id'];
    $sessions = self::get_sessions($id);

    if ($sessions === false)
      return logw("no_sessions_found_for:${id}", __CLASS__);

    $sessions[$id] = $session;

    $filename = self::filename_by_id($id);
    if (!$filename) return false;
    return ini::encode_file($filename, $sessions);
  }

  # delete session from file
  static function delete(string $id): bool
  {
    $sessions = self::get_sessions($id);
    if ($sessions === false)
      return logw("no_sessions_found_for:${id}", __CLASS__);

    if (isset($sessions[$id]))
      unset($sessions[$id]);

    $filename = self::filename_by_id($id);
    if (!$filename) return false;
    return ini::encode_file($filename, $sessions);
  }
}