<?php namespace api;

use \api\{users,session};

class auth
{
  # try to login user by username and password
  # this will create session for user, and set cookie
  # if successful it will @return \api\user
  static function login(string $username, string $password)
  {
    # get user if exists, valid username
    $user = users::get($username);
    if (!$user)
      return logw("no_such_user:${username}", __CLASS__);

    # match password
    if (!$user->password_verify($password)) {
      $user->log('api.auth.login', user::log_sec, 'invalid_password');
      return logw("invalid_password_for:${username}", __CLASS__);
    }

    $session = session::generate($user->id);
    if (!session::write($session)) {
      $user->log('api.auth.login', user::log_err, 'failed_to_write_session');
      return logw("failed_to_write_session_for:${username}", __CLASS__);
    }

    $user->session = $session;
    $user->log('api.auth.login', user::log_ok, $session['id']);

    return $user;
  }

  # restore user by session (if session sent in header)
  static function restore()
  {
    $auth = req()->header('authorization');
    if (!$auth)
      return logi('no_authorization_headers_found', __CLASS__) && false;

    list($type, $id) = explode(' ', $auth);

    if (!$type || $type !== 'Basic' || !$id)
      return logw('no_or_invalid_authorization_type_id', __CLASS__);

    $session = session::get($id);

    if (!$session) return logi("no_session_found_for:${id}", __CLASS__);

    $user = users::get_by_id($session['userid']);
    if (!$user)
      return logw("no_user_for_session:${id}(".$session['userid'].")", __CLASS__);

    $user->session = $session;
    $user->log('api.auth.resotre', user::log_ok, $session['id']);

    return $user;
  }

  # delete user's session
  static function logout()
  {
    $user = r('api.user');
    if (!$user) return logw('no_user_to_be_logedout', __CLASS__);
    $sessionid = $user->session['id'];
    if (!$sessionid) return logw('no_session_id_for_user:'.$user->id, __CLASS__);
    return session::delete($sessionid);
  }
}