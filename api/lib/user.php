<?php namespace api;

use \sys\ini;
use \sys\fs\{file};

class user
{
  const log_ok = 'OK';
  const log_warn = 'WARN';
  const log_err = 'ERR';
  const log_sec = 'SEC';

  function __construct(array $data)
  {
    $this->data = $data;
  }

  function password_verify(string $password): bool
  {
    return password_verify($password, $this->password);
  }

  function log($id, $status=self::log_ok, $message='')
  {
    $date = date('c');
    $ip = MYSLI_RUN_TYPE === 'INDEX' ? req()->ip() : 'cli';
    $line = "${id}  [${status}]  ${date}  ${ip}  [[${message}]]";
    return users::log_add($this->id, $line);
  }

  function save(): bool
  {
    return users::write($this);
  }

  function delete(bool $soft=true)
  {
    return users::remove($this);
  }

  function add_to_group(string $group)
  {
    $this->data['groups'][] = $group;
  }

  function __get(string $name) {
    if ($name === 'data') return $this->data;
    return array_key_exists($name, $this->data) ? $this->data[$name] : null;
  }

  function __set(string $name, $value) {
    if ($name === 'data') { $this->data = $value; }
    else { $this->data[$name] = $value; }
  }

  function record() {
    $data = $this->data;
    unset($data['password']);
    return $data;
  }
}