<?php namespace api\controller;

class api_menu extends \sys\controller
{
  function get_menu()
  {
    return [
      [
        'title' => 'Pages',
        'icon' => 'pencil alt',
        'action' => 'api.pages'
      ],
      [
        'title' => 'Settings',
        'icon' => 'cog',
        'action' => 'api.settings'
      ]
    ];
  }
}