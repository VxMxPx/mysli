<?php namespace api\controller;

use \sys\{str, pkg};

class api extends \sys\controller
{
  function resolve($params)
  {
    if (!count($params)) return api_error(t('api.error.no_parameters'));
    $method = strtolower(req()->method());
    $data = $method === 'get' ? req()->get() : req()->post();
    $segments = [];
    foreach($params as $i => $param) $params[$i] = str::clean($param, 'slug');

    if (count($params) === 1) {
      $id = $params[0];
      $class = "\\api\\controller\\api_${id}";
      $call = "${method}_${id}";
      $perm = "api.{$id}.{$call}";
    } elseif (count($params) === 2) {
      $pkg = $params[0];
      $id = $params[1];
      $class = "\\${pkg}\\controller\\api_${id}";
      $call = "${method}_${id}";
      $perm = "{$pkg}.{$id}.{$call}";
    } elseif (count($params) >= 3) {
      $pkg = $params[0];
      $id = $params[1];
      $fn = $params[2];
      $segments = array_splice($params, 3);
      $class = "\\${pkg}\\controller\\api_${id}";
      $call = "${method}_${fn}";
      $perm = "{$pkg}.{$id}.{$call}";
    }

    if (isset($pkg) && !pkg::is_enabled($pkg)) return api_error(t('api.error.no_such_package'));

    if (!class_exists($class)) return api_error(t('api.error.no_such_class'));
    $obj = new $class();
    if (!method_exists($obj, $call)) return api_error(t('api.error.no_such_method'));

    # verify that user actually can access the resource
    $user = r('api.user');
    if (!allowed($perm)) {
      return api_denied();
    }

    return $obj->{$call}($data, $segments);
  }
}

# man
# ---
# API URL structure:
# ---
# Two (2) segment URLs (i.e. /api/auth) are translated to internal API calls:
#   GET /api/auth => \api\controller\api_auth->get_auth
#   DELETE /api/auth => \api\controller\api_auth->delete_auth
# Permission example path: auth.delete_auth
# --
# Three (3) segment URLs (i.e. /api/dash/config) are translated to package calls:
#   GET /api/dash/config => \dash\controller\api_config->get_config()
# Permission example path: dash.config.get_config
# --
# Four (4) segment calls are translated to method calls within package:
#   GET /api/dash/config/users => \dash\controller\api_config->get_users
# Permission example path: dash.config.get_users
# --
# The request method is prefixed to the called class method:
#   GET /api/auth => \api\controller\api_auth->get_auth
#   PUT /api/auth => \api\controller\api_auth->put_auth
# --
# There's no deeper directory structure, would package author desire to handle
# calls to the package in a unique way, an API controller can be created:
#   GET /api/dash/config/users => \dash\controller\api.php
# The controller will be constructed, and `resolve` method will be called.
# --
# If request method is post|delete|put then post data is send to the call.
# If request method is get then get data is sent to the call.
# The leftover segments (five of more) are send to the call as as the second property:
#   $obj->post_name($data, $segments)
#   $obj->get_name($data, $segments)
