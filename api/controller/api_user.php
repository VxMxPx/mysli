<?php namespace api\controller;

use \api\users;
use \api\allow;

class api_user extends \sys\controller
{
  function get_user()
  {
    $id = req()->get('id');
    if (!$id || strpos($id, '.')) return api_error('no_id_provided');

    $user = users::get_by_id($id);
    if (!$user) return api_404('no_such_user');

    return $user->record();
  }

  function delete_user($data) {}
  function put_user($data) {}
  function post_user($data) {}
}
