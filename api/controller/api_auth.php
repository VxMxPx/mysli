<?php namespace api\controller;

use \api\auth;

class api_auth extends \sys\controller
{
  function post_auth($data)
  {
    if (r('api.user'))
      return api_error(t('api.error.already_loggedin'));

    $username = $data['username'];
    $password = $data['password'];

    if (!$username || !$password)
      return api_error(t('api.error.general_error'));

    $user = auth::login($username, $password);

    return $user ? $user->record() : api_denied(t('api.error.invalid_username_password'));
  }

  function get_auth()
  {
    $user = r('api.user');
    if ($user) {
      return $user->record();
    } else {
      return api_404(t('api.error.no_session'));
    }
  }

  function delete_auth()
  {
    return auth::logout();
  }
}
