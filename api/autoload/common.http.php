<?php

use \sys\ini;

function api_404($message)
{
  return api_error($message, 404);
}

function api_error($message, $code=500, $type='error')
{
  res()->set_status($code);
  return [
    'type' => $type,
    'message' => $message
  ];
}

function api_denied(string $message='')
{
  res()->set_status('401');
  return [
    'type' => 'error',
    'message' => $message ? $message : t('api.error.access_denied')
  ];
}

# check if permission is allowed
function allowed(string $permission): bool
{
  static $loaded;
  static $groups;

  if (!$loaded) $loaded = [];
  if (!$groups) {
    $groups_file = datpath('etc/groups.ini');
    $groups = ini::decode_file($groups_file);
  }

  $segments = explode('.', $permission);
  list($pkg, $id, $name) = $segments;

  if (!isset($loaded[$pkg]))
  {
    # read in package
    $path = pkgpath($pkg, 'etc/permissions.ini');

    # package does not define permissions, so allow it
    if (!file_exists($path)) return true;

    $perms = ini::decode_file($path);
    $loaded[$pkg] = $perms;
  }

  # if permission doesn't exists it must be allowed
  $perms = $loaded[$pkg];
  $has = isset($perms[$id]) && isset($perms[$id][$name]);
  if (!$has) return true;

  # permission is defined, so user needs to exists
  $user = r('api.user');
  if (!$user) return false;

  # check if user is allowed to access
  $allowed = false;
  foreach($user->groups as $group_id)
  {
    if (isset($groups[$group_id]))
    {
      $group = $groups[$group_id];
      # if permissions is specifically set to be true or false,
      # use this value rather than the default value of a group
      if (isset($group['permissions']) && isset($group['permissions'][$permission])) {
        if ($group['permissions'][$permission]) {
          $allowed = true;
          break;
        }
      } elseif ($group['default']) {
        $allowed = true;
        break;
      }
    }
  }

  return $allowed;
}
