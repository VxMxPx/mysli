<?php namespace api;

# run route
$router = r('router');

$router->any('/api/...params?', function($params=[]) {
  # restore session if there (authorization header)
  r('api.user', auth::restore());

  # get controller
  $api = new controller\api();
  $res = $api->resolve($params);
  if (is_bool($res)) $res = ['status' => $res];
  return res($res);
});