<?php namespace api\etc;

use \sys\fs\{file,dir};
use \api\users;

class setup
{
  # default roles
  static $groups_ini = <<<GROUPS_INI
  [guest]
  name=Guest
  default=deny

  [root]
  name=Root
  default=allow
  GROUPS_INI;

  static function enable()
  {
    # directories to be created
    $dirs = [
      datpath('etc/users'),
    ];

    foreach($dirs as $dir)
      if (!is_dir($dir)) { mkdir($dir, 0777, true); }

    # write roles
    $files = [
      datpath('etc/groups.ini') => static::$groups_ini
    ];

    foreach($files as $file_path => $file_contents) {
      if (!file::exists($file_path))
        file::write($file_path, $file_contents) || err('cannot_write_file', $file_path);
    }

    if (users::exists('root@localhost')) users::remove('root@localhost');
    $user = users::create('root@localhost', 'root');
    $user->add_to_group('root');
    $user->save();

    return true;
  }
}
