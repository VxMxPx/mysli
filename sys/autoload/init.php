<?php

namespace sys;

function load_core_util()
{
  $ds = DIRECTORY_SEPARATOR;

  # Load toolki and dat core util
  $libpaths = [
    MYSLI_PKGPATH.$ds.'sys'.$ds.'autoload-core',
    MYSLI_DATPATH.$ds.'autoload-core',
  ];
  foreach($libpaths as $libpath)
  {
    if (!is_dir($libpath)) { continue; }
    $libs = scandir($libpath);
    foreach($libs as $lib)
    {
      if (substr($lib, -4) === '.php') { include $libpath.$ds.$lib; }
    }
  }
}

function load_core_libraries(array $libraries)
{
  foreach ($libraries as $lib)
  {
    $f = pkgpath('sys', 'lib', $lib.'.php');
    if (file_exists($f)) include $f;
    else trigger_error("Cannot load, not found: `$lib` in `$f`.", E_USER_ERROR);
  }
}

function run_autoload(array $packages)
{
  foreach ($packages as $pkg)
  {
    $pkg_al = pkgpath($pkg, 'autoload');
    if (!is_dir($pkg_al)) continue;
    foreach (scandir($pkg_al) as $file)
    {
      if (substr($file, -4) === '.php') {

        # Skip if limited autoload
        if ((substr($file, -9) !== '.http.php' && substr($file, -8) !== '.any.php') && defined('MYSLI_INDEX')) { continue; }
        if ((substr($file, -8) !== '.cli.php' && substr($file, -8) !== '.any.php') && defined('MYSLI_CLI'))   { continue; }

        $root_f = ds($pkg_al, $file);
        log::info("I'm about to load (from run_autoload): `{$root_f}`.", __CLASS__);
        include $root_f;
      }
    }
  }
}

class toolkit
{
  function shutdown()
  {
    event::fire('stop@sys');
  }
}

call_user_func(function() {

  define('MYSLI_PKGDIRNAME', 'pkg');

  # Load autoload.core libraries
  load_core_util();

  # Load own libraries
  load_core_libraries(['err', 'autoloader', 'ini', 'pkg', 'math', 'str', 'arr']);

  # Register autoloader
  spl_autoload_register('\sys\autoloader::load');

  # Constants
  defined('sys_ym_indent') || define('sys_ym_indent', 2);

  # Before init
  event::fire('before.init@sys');

  # Register instances
  $cfg = r('cfg', new cfg());
  $sys = r('sys', new toolkit());

  # Load always-load list
  run_autoload(['dat']);
  run_autoload(pkg::map(function($pkg) { return $pkg->id; }, pkg::LIST_ENABLED));

  # After init
  event::fire('after.init@sys');

  return true;
});
