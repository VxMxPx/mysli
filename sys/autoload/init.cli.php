<?php

namespace sys;

use \sys\{pkg, event};
use \sys\cli\output as out;

function get_scripts(): array
{
  $packages = pkg::enabled();
  datpkg($packages);
  $scripts  = [];

  foreach($packages as $pkg)
  {
    $dir = pkgpath($pkg->id, 'command');
    if (!is_dir($dir)) { continue; }

    $files = scandir($dir);

    foreach ($files as $file)
    {
      if (substr($file, -4) !== '.php')
      {
        continue;
      }
      $script = substr($file, 0, -4);
      $sid = $script;
      if (isset($scripts[$sid])) { $sid = "{$pkg->id}.{$sid}";
        if (isset($scripts[$sid])) err('duplicate_script', $sid);
      }

      $scripts[$sid] = [
        'id'      => $sid,
        'fid'     => ltrim("{$pkg->id}.{$script}", '.'),
        'script'  => $script,
        'package' => $pkg,
        'meta'    => $pkg->meta,
        'path'    => pkgpath($pkg->id, 'command', $file),
        'class'   => filetoclass(ds($pkg->id, 'command', $file))
      ];
    }
  }

  return $scripts;
}

function execute_command(array $arguments)
{
  if (!array_key_exists(1, $arguments))
  {
    list_commands();
    return;
  }
  else
  {
    run_command(array_slice($arguments, 1));
    return;
  }
}

function run_command(array $arguments)
{
  $commands = get_scripts();
  $call = array_shift($arguments);
  $found = false;

  foreach($commands as $command)
  {
    if ($command['fid'] === $call || $command['id'] === $call)
    {
      (new $command['class']($arguments))->run($arguments);
      $found = true;
    }
  }

  if (!$found) {
    out::error("No such command: {$call}");
  }
}

function list_commands()
{
  $scripts = get_scripts();
  out::line('List of available commands:');
  out::nl();
  out::ul(array_keys($scripts));
}

call_user_func(function() {
  global $argv;
  event::fire('before.init@sys.init.cli');
  execute_command($argv);
  event::fire('after.init@sys.init.cli');
});
