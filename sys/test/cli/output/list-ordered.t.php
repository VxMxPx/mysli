<?php

#: Before
use \sys\cli\output;

# ------------------------------------------------------------------------------
#: Test List Ordered
#: Expect Output <<<CLI
output::ol(['One', 'Two', 'Three', 'Four']);
<<<CLI
1. One
2. Two
3. Three
4. Four

CLI;

# ------------------------------------------------------------------------------
#: Test List Ordered Indented
#: Expect Output <<<CLI
output::ol(['One', 'Two', 'Three', 'Four'], 1);
<<<CLI
  1. One
  2. Two
  3. Three
  4. Four

CLI;

# ------------------------------------------------------------------------------
#: Test List Ordered Multi Dimensions
#: Expect Output <<<CLI
output::ol([
    'One',
    [ 'A', 'B', 'C' ],
    'Two',
    'Three',
    'Four'
]);
<<<CLI
1. One
  1. A
  2. B
  3. C
2. Two
3. Three
4. Four

CLI;
