<?php

#: Before
use \sys\cli\output;

# ------------------------------------------------------------------------------
#: Test List Aligned
#: Expect Output <<<CLI
output::al([
  'One' => 'Value One',
  'Two' => 'Value Two',
  'Three' => 'Value Three',
  'Four' => 'Value Four',
]);
<<<CLI
One   : "Value One"
Two   : "Value Two"
Three : "Value Three"
Four  : "Value Four"

CLI;

# ------------------------------------------------------------------------------
#: Test List Aligned Indented
#: Expect Output <<<CLI
output::al([
  'One' => 'Value One',
  'Two' => 'Value Two',
  'Three' => 'Value Three',
  'Four' => 'Value Four',
], 1);
<<<CLI
  One   : "Value One"
  Two   : "Value Two"
  Three : "Value Three"
  Four  : "Value Four"

CLI;

# ------------------------------------------------------------------------------
#: Test List Aligned Multi Dimensions
#: Expect Output <<<CLI
output::al([
  'One' => 'Value One',
  'Two' => [
    'One' => 'Value One',
    'Two' => 'Value Two',
    'Three' => 'Value Three',
    'Four' => 'Value Four',
  ],
  'Three' => 'Value Three',
  'Four' => 'Value Four',
]);
<<<CLI
One   : "Value One"
Two
  One   : "Value One"
  Two   : "Value Two"
  Three : "Value Three"
  Four  : "Value Four"
Three : "Value Three"
Four  : "Value Four"

CLI;
