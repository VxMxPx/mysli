<?php

#: Before
use \sys\cli\output;

# ------------------------------------------------------------------------------
#: Test Strong Line
#: Expect Output <<<CLI
output::strong('Hello!');
<<<CLI
[1mHello![0m

CLI;
