<?php

#: Before
use \sys\cli\output;

# ------------------------------------------------------------------------------
#: Test Indent
#: Expect Output <<<CLI
output::indent('One', 1);
output::indent('Two', 2);
output::indent('Three', 3);
output::indent('Four');
<<<CLI
  One
    Two
      Three
Four

CLI;
