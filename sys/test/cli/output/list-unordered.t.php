<?php

#: Before
use \sys\cli\output;

# ------------------------------------------------------------------------------
#: Test List Unordered
#: Expect Output <<<CLI
output::ul(['One', 'Two', 'Three', 'Four']);
<<<CLI
- One
- Two
- Three
- Four

CLI;

# ------------------------------------------------------------------------------
#: Test List Unordered Indented
#: Expect Output <<<CLI
output::ul(['One', 'Two', 'Three', 'Four'], 1);
<<<CLI
  - One
  - Two
  - Three
  - Four

CLI;

# ------------------------------------------------------------------------------
#: Test List Unordered Multi Dimensions
#: Expect Output <<<CLI
output::ul([
    'One',
    [ 'A', 'B', 'C' ],
    'Two',
    'Three',
    'Four'
]);
<<<CLI
- One
  - A
  - B
  - C
- Two
- Three
- Four

CLI;
