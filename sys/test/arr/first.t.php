<?php

#: Before
use sys\arr;

#: Test Basic
#: Expect String "hello"
return arr::first(['hello', 'world']);

#: Test Empty
#: Expect Null
return arr::first([]);
