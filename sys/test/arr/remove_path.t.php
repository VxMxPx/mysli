<?php

#: Before
use sys\arr;

#: Define Data
$array = [
    'main' => [
        'settings' => [
            'timezone' => 'GMT',
            'debug'    => true
        ]
    ]
];

#: Test Basic
#: Use Data
return assert::equals(
    arr::remove_path($array, 'main.settings.timezone'),
    [
        'main' => [
            'settings' => [
                'debug' => true
            ]
        ]
    ]
);

#: Test Remove Array
#: Use Data
return assert::equals(
    arr::remove_path($array, 'main.settings'),
    [
        'main' => []
    ]
);

#: Test Remove Nothing
#: Use Data
$original = $array;
return assert::equals(
    arr::remove_path($array, ''),
    $original
);

#: Test Non-Existent Key
#: Use Data
$original = $array;
return assert::equals(
    arr::remove_path($array, 'main.not_found'),
    $original
);
