<?php

#: Before
use sys\arr;

#: Test Basic
#: Expect String "world"
return arr::last(['hello', 'world']);

#: Test Empty
#: Expect Null
return arr::last([]);
