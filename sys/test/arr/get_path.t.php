<?php

#: Before
use sys\arr;

#: Define Data
$array = [
    'main' => [
        'settings' => [
            'timezone' => 'GMT',
            'debug'    => true
        ],
        'pets' => [
            'bunny',
            'cat' => [ 'Riki', 'Zitalik' ],
            'dog'
        ]
    ]
];

#: Test Boolean Value
#: Use Data
#: Expect True
return arr::get_path($array, 'main.settings.debug');

#: Test String Value
#: Use Data
#: Expect String "GMT"
return arr::get_path($array, 'main.settings.timezone');

#: Test Array Value
#: Use Data
return assert::equals(
    arr::get_path($array, 'main.settings'),
    [ 'timezone' => 'GMT', 'debug' => true ]
);

#: Test Missing, Default
#: Use Data
#: Expect String "Default"
return arr::get_path($array, 'main.settings.not_there', 'Default');

#: Test Numeric
#: Use Data
#: Expect String "bunny"
return arr::get_path($array, 'main.pets.0');

#: Test Numeric, Deeper
#: Use Data
#: Expect String "Zitalik"
return arr::get_path($array, 'main.pets.cat.1');

#: Test Empty
#: Expect Null
return arr::get_path([], 'main.pets.cat.0');
