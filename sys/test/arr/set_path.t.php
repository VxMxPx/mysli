<?php

#: Before
use sys\arr;

#: Define Data
$array = [
    'main' => [
        'settings' => [
            'timezone' => 'GMT',
            'debug'    => true
        ]
    ]
];
$pets = [
    'pets' => [ 'dog', 'cat', 'bunny' ]
];

#: Test Basic
#: Use Data
$modified = $array;
$modified['main']['settings']['timezone'] = 'GMT+2';
return assert::equals(
    arr::set_path($array, 'main.settings.timezone', 'GMT+2'),
    $modified);

#: Test Non-Associative
#: Use Data
return assert::equals(
    arr::set_path($pets, 'pets', ['turtle', 'piglet']),
    [
        'pets' => [ 'turtle', 'piglet', 'bunny' ]
    ]
);

#: Test Specific in Non-Associative
#: Use Data
return assert::equals(
    arr::set_path($pets, 'pets.2', 'rabbit'),
    [
        'pets' => [ 'dog', 'cat', 'rabbit' ]
    ]
);

#: Test Specific in Non-Associative to Array
#: Use Data
return assert::equals(
    arr::set_path($pets, 'pets.2', ['fluffy', 'bunny']),
    [
        'pets' => [ 'dog', 'cat', [ 'fluffy', 'bunny' ] ]
    ]
);

#: Test New Key in Root
#: Use Data
$modified = $array;
$modified['number'] = 42;
return assert::equals(arr::set_path($array, 'number', 42), $modified);

#: Test New Empty Key in Root
#: Use Data
$modified = $array;
$modified[''] = 43;
return assert::equals(arr::set_path($array, '', 43), $modified);

#: Test Modify Multiple by General Key
#: Use Data
$modified = $array;
$modified['main']['settings']['timezone'] = 'GMT+2';
$modified['main']['settings']['debug']    = -1;
$array = arr::set_path($array, 'main.settings', [ 'timezone' => 'GMT+2', 'debug' => -1 ]);
return assert::equals($array, $modified);
