<?php

#: Before
use sys\fs\dir;

#: Test Basic
$dirlist = [];
dir::map_up(
  '/home/user/Projects/mysli.dev/sys/base',
  function($path, $cut) use (&$dirlist) {
    $dirlist[$cut] = $path;
  });
return assert::equals($dirlist, [
  'base' => '/home/user/Projects/mysli.dev/sys/',
  'sys/base' => '/home/user/Projects/mysli.dev/',
  'mysli.dev/sys/base' => '/home/user/Projects/',
  'Projects/mysli.dev/sys/base' => '/home/user/',
  'user/Projects/mysli.dev/sys/base' => '/home/',
  'home/user/Projects/mysli.dev/sys/base' => '/',
]);

#: Test Relative
$dirlist = [];
dir::map_up(
  'mysli.dev/sys/base',
  function($path, $cut) use (&$dirlist) {
    $dirlist[$cut] = $path;
  });
return assert::equals($dirlist, [
  'base' => 'mysli.dev/sys/',
  'sys/base' => 'mysli.dev/',
]);

#: Test Double Slash
$dirlist = [];
dir::map_up(
  '//mysli.dev/sys/base',
  function($path, $cut) use (&$dirlist) {
    $dirlist[$cut] = $path;
  });
return assert::equals($dirlist, [
  'base' => '/mysli.dev/sys/',
  'sys/base' => '/mysli.dev/',
  'mysli.dev/sys/base' => '/'
]);

#: Test Backslashes
$dirlist = [];
dir::map_up(
  'mysli.dev\\sys\\base',
  function($path, $cut) use (&$dirlist) {
    $dirlist[$cut] = $path;
  });
return assert::equals($dirlist, [
  'base' => 'mysli.dev/sys/',
  'sys/base' => 'mysli.dev/',
]);

#: Test No-slashes
$dirlist = ['empty'];
dir::map_up(
  'directory',
  function($path, $cut) use (&$dirlist) {
    $dirlist[$cut] = $path;
  });
return assert::equals($dirlist, ['empty']);