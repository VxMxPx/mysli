<?php

#: Before
use sys\math;

#: Test Basic
#: Expect Integer 25
return math::set_percent(25, 100);

#: Test Float
#: Expect Float 16.12
return math::set_percent(13, 124);
