<?php

#: Before
use sys\ini;

#: Test General Array
$encoded = ini::encode([ 'name' => 'Mysli', 'exists' => true ]);
return assert::equals($encoded, <<<EXPECT
name=Mysli
exists=true

EXPECT
);

#: Test Empty Array
$encoded = ini::encode([]);
return assert::equals($encoded, <<<EXPECT

EXPECT
);

#: Test Multi Dimensional Array
$encoded = ini::encode([
  'id' => 'sys',
  'categories' => ['system', 'pkg']
]);
return assert::equals($encoded, <<<EXPECT
id=sys

[categories]
0=system
1=pkg


EXPECT
);

#: Test Multi Dimensional Array, No Categories
$encoded = ini::encode([
  'id' => 'sys',
  'group' => ['system', 'pkg']
], false);
return assert::equals($encoded, <<<EXPECT
id=sys
group=system,pkg

EXPECT
);

#: Test Multi Dimensional Array, Reordered
$encoded = ini::encode([
  'id' => 'sys',
  'group' => ['system', 'pkg'],
  'created_on' => 2019
]);
return assert::equals($encoded, <<<EXPECT
id=sys
created_on=2019

[group]
0=system
1=pkg


EXPECT
);