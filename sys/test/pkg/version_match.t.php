<?php

#: Before
use sys\pkg;

#: Test Full
#: Use String
#: Expect True
return pkg::version_match('1.1.0', '1.1.0');

#: Test Bigger Bugfix
#: Use String
#: Expect True
return pkg::version_match('1.1.0', '1.1.50');

#: Test Mid Format
#: Use String
#: Expect True
return pkg::version_match('1.1', '1.1.0');

#: Test Short Format
#: Use String
#: Expect True
return pkg::version_match('1', '1.1.0');

#: Test Miss Bugfix
#: Use String
#: Expect False
return pkg::version_match('1.1.1', '1.1.0');

#: Test Miss Feature
#: Use String
#: Expect False
return pkg::version_match('1.1.0', '1.0.0');

#: Test Miss Breaking
#: Use String
#: Expect False
return pkg::version_match('1.0.0', '2.1.0');
