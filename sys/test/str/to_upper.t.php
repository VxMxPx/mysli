<?php

#: Before
use sys\str;

#: Test Basic
#: Expect String "ŠĐŽČĆŽ"
return str::to_upper('šđžčćž');
