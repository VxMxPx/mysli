<?php

#: Before
use sys\str;

#: Test Basic
#: Expect String "šđžčćž"
return str::to_lower('ŠĐŽČĆŽ');
