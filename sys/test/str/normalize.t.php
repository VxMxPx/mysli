<?php

#: Before
use sys\str;

#: Test Basic
#: Expect String "SDZCC-sdzcc"
return str::normalize('ŠĐŽČĆ-šđžčć');
