<?php

use \sys\{ini, pkg};

function t(string $key, $default=null)
{
  static $r;
  $cfg = r('cfg');
  $l = $cfg->get('language', 'en');

  # split key
  $s = explode('.', $key);

  if (!is_array($r)) { $r = []; }

  # needs to be loaded?
  if (!isset($r[$s[0]]))
  {
    $r[$s[0]] = [];
    $fpkg = pkgpath($s[0], "i18n/{$l}.ini");
    $fdat = pkgpath('dat', "i18n/{$s[0]}.{$l}.ini");
    $apkg = $adat = [];
    file_exists($fpkg) and $apkg = ini::decode_file($fpkg);
    file_exists($fdat) and $adat = ini::decode_file($fdat);
    $r[$s[0]] = array_merge($apkg, $adat);
  }

  return arr_path($r, $key, $default?$default:$key);
}
