<?php

# get configuration object
function cfg(string $get=null, $default=null)
{
  static $cfg;
  if (!$cfg) $cfg = r('cfg');
  return $get ? $cfg->get($get, $default) : $cfg;
}

# trigger an exception throw
function err(string $id, string $message=null, int $code=0)
{
  \sys\err::t($id, $message, $code);
}

# super shortcuts to logs
function logx(string $m, string $f='') { \sys\log::error($m, $f); return false; }
function logw(string $m, string $f='') { \sys\log::warn($m, $f);  return false; }
function logi(string $m, string $f='') { \sys\log::info($m, $f);  return false; }
