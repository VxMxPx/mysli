<?php

function r(string $id, $instance=null)
{
  static $instances;
  if (is_null($instance)) return isset($instances[$id]) ? $instances[$id] : null;
  else return $instances[$id] = $instance;
}
