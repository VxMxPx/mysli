<?php

# correct directory separators
function ds(...$path): string
{
  $path = array_filter($path, 'strlen');
  $path = implode(DIRECTORY_SEPARATOR, $path);

  return $path
    ? preg_replace('/(?<![:\/])[\/\\\\]+/', DIRECTORY_SEPARATOR, $path) : '';
}

function urlseperator(...$path): string
{
  $path = array_filter($path, 'strlen');
  $path = implode('/', $path);

  return $path
    ? preg_replace('/(?<![:\/])[\/\\\\]+/', '/', $path) : '';
}

# get various system paths
function pkgpath(...$p): string
{
  return (array_key_exists(0, $p) && $p[0] === 'dat')
    ? datpath(...array_slice($p, 1))
    : ds(MYSLI_PKGPATH, ...$p);
}
function datpath(...$p): string { return ds(MYSLI_DATPATH, ...$p); }
function pubpath(...$p): string { return ds(MYSLI_PUBPATH, ...$p); }
function tmppath(...$p): string { return ds(MYSLI_DATPATH, 'tmp', ...$p); }

# return or insert data dummy package info
function datpkg(array &$packages=null): \sys\package
{
  $pkg = new \sys\package([
    'id'         => 'dat',
    'version'    => '1.0.0',
    'date'       => '2017-06-10',
    'updated_on' => '2017-06-10',
    'require'    => [],
    'enabled'    => true
  ]);

  if ($packages) {
    if (array_key_exists(0, $packages)) {
      $packages[] = $pkg;
    }
    else {
      $packages[$pkg->id] = $pkg;
    }
  }

  return $pkg;
}

# convert filepath to class:
#   sys.cli.output     => \sys\cli\output
#   sys/cli/output.php => ^
function filetoclass(string $path): string
{
  if (substr($path, -4) === '.php') { $path = substr($path, 0, -4); }
  return '\\'.str_replace(['.', '/'], '\\', $path);
}

# determine if script is running in command line
function is_cli()
{
  return php_sapi_name() === 'cli' || defined('STDIN');
}

# output a variable as: <pre>print_r($variable)</pre> (this is only for debugging)
# this will die after dumping variables on screen
function dump()
{
  die(call_user_func_array('dump_r', func_get_args()));
}

# dump, but don't die - echo results instead
function dump_e()
{
  echo call_user_func_array('dump_r', func_get_args());
}

# dump, but don't die - fwrite STDOUT results
function dump_s()
{
  if (!defined('STDOUT')) {
    trigger_error("Cannot use `dump_s` when not running in CLI!", E_USER_ERROR);
  }
  fwrite(STDOUT, call_user_func_array('dump_r', func_get_args()));
  fwrite(STDOUT, PHP_EOL);
  flush();
}

# dump, but don't die - return results instead
function dump_r()
{
  $arguments = func_get_args();
  $result = '';

  foreach ($arguments as $variable)
  {
    if (is_bool($variable)) {
      $bool = $variable ? 'true' : 'false';
    } else {
      $bool = false;
    }

    $result .= (!is_cli()) ? "\n<pre>\n" : "\n";
    $result .= '' . gettype($variable);
    $result .= (is_string($variable) ? '['.strlen($variable).']' : '');
    $result .=  ': ' . (is_bool($variable) ? $bool : print_r($variable, true));
    $result .= (!is_cli()) ? "\n</pre>\n" : "\n";
  }

  return $result;
}

# query array element by a string path. my.key.value
function arr_path($arr, $path, $default=null)
{
  $path = trim($path, '.');
  $path = explode('.', $path);
  $get  = $arr;
  foreach ($path as $w)
  {
    if (is_array($get) && array_key_exists($w, $get)) $get = $get[$w];
    else return $default;
  }
  return $get;
}

# create a designated scope, example:
# $sum = using(12, 40, function($a, $b) { return $a + $b; });
# return callback's results
function using(...$args)
{
  $f = array_pop($args);
  return call_user_func_array($f, $args);
}

# format generic exception message when parsing string
# if $lines are array `err_lines` will be called, otherwise `err_char`
function f_error($lines, int $current, string $message, string $file=null): string
{
  return $message . "\n" .
    (is_array($lines)
      ? err_lines($lines, $current, 3)
      : err_char($lines, $current)).
    ($file ? "File: `{$file}`\n" : "\n");
}

# return -$padding, $current, +$padding lines for exceptions, for example:
#   11. ::if true
# >>12.     {username|non_existant_function}
#   13. ::/if
# $current use negative to get lines from end of the list
function err_lines(array $lines, int $current, int $padding=3): string
{
  if ($current < 0) $current = count($lines)+$current;

  $start  = $current - $padding;
  $end    = $current + $padding;
  $result = '';

  for ($position = $start; $position <= $end; $position++)
  {
    if (isset($lines[$position])) {
      $result .= $position === $current ? '>>' : '  ';
      $result .= ($position+1).". {$lines[$position]}\n";
    }
  }

  return $result;
}

# mark particular character in line
# example:
# There's an error I require dot.
# ----------------^--------------
# $line full line
# $at which character error occurred
function err_char(string $line, string $at): string
{
    $output  = $line."\n";
    $output .= str_repeat(' ', $at);
    $output .= '^';
    return $output;
}

# output logs as an HTML
function log_as_html(array $logs): string
{
  $css_output = "width:100%;";
  $css_message = "width:100%; background:#234; color:#eee;";

  $output = <<<STYLE
  <style type="text/css">
    section.logs
    {
      width: 100%;
    }
    section.logs div.log-message
    {
      display: block;
      background: #0F181A;
      color: #999;
      border-bottom: 1px solid #444;
      padding: 10px;
      font-family: sans;
      font-size: 12px;
    }
    section.logs div.log-message span.message
    {
      font-family: monospace;
      font-size: 16px;
      display: block;
      margin-bottom: 10px;
    }
    section.logs div.log-message.type-debug span.message
    {
      color: #6e973d;
    }
    section.logs div.log-message.type-info span.message
    {
      color: #aee87b;
    }
    section.logs div.log-message.type-notice span.message
    {
      color: #ddb691;
    }
    section.logs div.log-message.type-warning span.message
    {
      color: #ed683b;
    }
    section.logs div.log-message.type-error span.message
    {
      color: #f23d3d;
    }
    section.logs div.log-message.type-panic span.message
    {
      color: #fcc;
      background: #893434;
      padding: 5px;
      border-radius: 4px;
    }
    section.logs div.log-message.type-panic span.message:before
    {
      content: "PANIC!!";
      font-weight: bold;
      font-size: 18px;
      display: block;
    }
    section.logs div.log-message span.type
    {
      display: none;
    }
    section.logs div.log-message span.from
    {
      padding-right: 10px;
    }
  </style>
STYLE;

  $output .= '<section class="logs" />';

  foreach ($logs as $k => $log)
  {
    $output .= '<div class="log-message type-'.$log['type'].'">';
    $output .= '<span class="type">'.$log['type'].'</span>';
    $output .= '<span class="message">'.$log['message'].'</span>';
    $output .= '<span class="from">'.$log['from'].'</span>';
    $output .= '<span class="time">'.$log['timestamp'].'</span>';
    $output .= '</div>';
  }

  $output .= '</section>';

  return $output;
}
