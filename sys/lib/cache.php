<?php namespace sys;

use \sys\fs\dir;

class cache
{
  function __construct(string $id)
  {
    $this->id = $id;
    $this->qid = $this->id_to_qid($id);

    $cache_dir = tmppath('cache');
    if (!dir::exists($cache_dir)) {
      dir::create($cache_dir);
    }
    $this->path = ds($cache_dir, $this->qid);
  }

  function id_to_qid(string $id): string
  {
    $dirty = basename($id).'-'.md5($id);
    $dirty = strtolower($dirty);
    $clean = str::clean($dirty, 'slug');
    return $clean.'.php';
  }

  function filename(): string
  {
    return $this->path;
  }

  function set(string $contents): bool
  {
    return !! fs\file::write($this->path, $contents);
  }

  function exists(): bool
  {
    return file_exists($this->path);
  }

  function erase(): bool
  {
    if ($this->exists()) {
      return unlink($this->path);
    }
    return true;
  }

  function get(): string
  {
    if ($this->exists()) {
      return file_get_contents($this->path);
    } else return '';
  }
}
