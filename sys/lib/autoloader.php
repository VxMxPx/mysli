<?php

namespace sys; class autoloader
{
  static function load(string $class): bool
  {
    // \sys\event => sys/event.php
    $file = str_replace('\\', '/', $class).'.php';
    // sys/event.php => sys
    $pkg = substr($file, 0, strpos($file, '/'));
    // sys/event.php => event.php
    $file = substr($file, strlen($pkg)+1);

    # Try for a library
    if (file_exists(pkgpath($pkg, 'lib', $file)))
    {
      include pkgpath($pkg, 'lib', $file);
      return true;
    }
    elseif (file_exists(pkgpath($pkg, $file)))
    {
      include pkgpath($pkg, $file);
      return true;
    }
    else
    {
      return false;
    }

  }
}
