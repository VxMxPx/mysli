<?php

namespace sys; class log
{
  static $items = [];

  static function add(string $type, string $message, string $from)
  {
    $item = [
      'timestamp' => gmdate('c'),
      'type'      => $type,
      'message'   => $message,
      'from'      => $from
    ];
    event::fire('add@sys.log', [&$item]);
    static::$items[] = $item;

    if (MYSLI_LOG_TO_CLI) {
      $line =
        strtoupper(substr($item['type'], 0, 3)).' '.
        $item['message'].' ['.$item['from'].']';
      error_log($line);
    }
  }

  # Dump all log messages.
  static function dump(): array { return static::$items; }

  # Shortcuts to logs.
  static function info   (string $message, string $from) { static::add('info',   $message, $from); }
  static function secure (string $message, string $from) { static::add('secure', $message, $from); }
  static function warn   (string $message, string $from) { static::add('warn',   $message, $from); }
  static function error  (string $message, string $from) { static::add('error',  $message, $from); }
  static function panic  (string $message, string $from) { static::add('panic',  $message, $from); }
}
