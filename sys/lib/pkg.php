<?php namespace sys;

use \sys\arr;
use \sys\fs\{dir,file};

class pkg
{
  static private $list;
  static private $list_enabled;

  const LIST_WRITE = true;
  const LIST_ENABLED = true;

  # reload packages list
  static function reload() {
    self::$list = [];
    self::$list_enabled = [];

    # grab all packages
    $packages = scandir(pkgpath());
    foreach ($packages as $id) {
      if (!file_exists(pkgpath($id, 'pkg.ini'))) { continue; }
      $meta = self::load_meta($id);
      $meta['enabled'] = false;
      self::$list[$id] = new \sys\package($meta);
    }

    # grab enabled packages list
    $list = datpath('etc/packages');
    if (!file_exists($list)) { err('packages_file_missing', $list); }
    else { $list = file($list); }

    foreach ($list as $line) {
      $line = trim($line);
      if (substr($line, 0, 1) === '#') { continue; }
      if (!$line || strpos($line, ';') < 1) { continue; }
      list($id, $version, $date) = explode(';', $line);
      $meta = [
        'id'        => $id,
        'version'   => $version,
        'date'      => $date,
        'enabled'   => true
      ];
      if (!isset(self::$list[$id])) err('package_enabled_but_not_found_in_filesystem', $id);
      $pkg = self::$list[$id];
      $pkg->update_meta($meta);
      self::$list_enabled[] = $pkg;
      self::$list[$id] = $pkg;
    }
  }

  # get ordered list of enabled packages
  static function enabled(): array {
    if (!self::$list_enabled) self::reload();
    return self::$list_enabled;
  }

  # get list of all package in no particular order
  static function list(): array {
    if (!self::$list) self::reload();
    return self::$list;
  }

  # filter all packages
  static function filter($callback, bool $enabled = false): array {
    $packages = $enabled ? self::enabled() : self::list();
    $list = array_filter($packages, $callback, ARRAY_FILTER_USE_BOTH);
    return $list;
  }

  # call a function for each package
  # $callback ($pkg)
  # $enabled map only enabled packages
  # return [ return_value, return_value ]
  static function map($callback, bool $enabled = false): array {
    $list = $enabled ? self::enabled() : self::list();
    $results = [];
    foreach($list as $pkg) $results[] = $callback($pkg);
    return $results;
  }

  # check if package exists
  static function exists(string $id): bool {
    return !! self::get($id);
  }

  # does package exists and is enabled
  static function is_enabled(string $id): bool {
    $pkg = self::get($id);
    return $pkg && $pkg->enabled;
  }

  # get package by ID, null if not found
  static function get(string $id): \sys\package {
    $list = self::list();
    return isset($list[$id]) ? $list[$id] : null;
  }

  # get package by namespace, i.e. sys.pkg => sys
  static function by_namespace(string $namespace): \sys\package {
    $id = ltrim(str_replace('\\', '.', $namespace), '.');
    if (substr($id, 0, 4) === 'dat.') return self::get('dat');
    else if (substr_count($id, '.') === 0) return self::get($id);
    else return self::get(substr($id, 0, strpos($id, '.')));
  }

  # read package's metadata
  static function load_meta(string $id): array {
    $f = pkgpath($id, 'pkg.ini');
    if (!file::exists($f)) err('no_metadata_file_for_package', $id);
    return ini::decode_file($f);
  }

  # match a version with a pattern
  # $required version number (ie 1.1) will match 1.1-inf.*
  static function version_match(string $required, string $actual): bool {
    $resolve = function($ver) {
      $ver = explode('.', $ver);
      $breaking = isset($ver[0]) ? (int) $ver[0] : 0;
      $feature = isset($ver[1]) ? (int) $ver[1] : 0;
      $bugfix = isset($ver[2]) ? (int) $ver[2] : 0;
      return [ $breaking, $feature, $bugfix ];
    };

    $required = $resolve($required);
    $actual = $resolve($actual);

    return
      $required[0] === $actual[0] &&
      $required[1] <= $actual[1] &&
      $required[2] <= $actual[2];
  }

  # physically remove package's data files from file system
  static function delete(string $id): bool {
    $dir = pkgpath($id);
    if (dir::exists($dir)) {
      return dir::remove($dir);
    }
  }

  # add package to the list of enabled packages
  static function list_add(string $id, bool $write = false): bool {
    if (!self::$list_enabled) self::init();
    self::$list_enabled[] = self::get($id);
    return $write ? self::list_write() : true;
  }

  # remove package from the list of enabled packages
  static function list_remove(string $id, bool $write = false): bool {
    foreach(self::$list_enabled as $index => $pkg) {
      if ($pkg->id === $id) {
        array_splice(self::$list_enabled, $index, 1);
        return $write ? self::list_write() : true;
      }
    }
    return false;
  }

  # write list of enabled packages
  static function list_write(): bool {
    $list = "# package;version;date\n";
    $packages = self::enabled();
    $nowdata = gmdate('c');
    foreach ($packages as $pkg) {
      $date = $pkg->date ? $pkg->date : $nowdata;
      $list .= "{$pkg->id};{$pkg->version};{$date}\n";
    }
    return !! file::write(datpath('etc/packages'), $list);
  }
}
