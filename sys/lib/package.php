<?php namespace sys;

class package {

  function __construct(array $meta) {
    $this->meta = $meta;
  }

  # will update (merge) current meta object with a new one
  function update_meta(array $meta) {
    $this->meta = array_merge($this->meta, $meta);
  }

  # enable package
  # $tree enable all packages that are required by this package
  function enable(bool $tree=true): bool {
    if ($this->enabled) err('already_enabled', $this->id);

    # enable packages tree
    if ($tree) {
      $required = $this->get_dependencies(true);
      if (count($required['missing'])) {
        err('missing_dependencies', $required['missing']);
      }
      if (count($required['disabled'])) {
        foreach ($required['disabled'] as $id) {
          $pkg = pkg::get($id);
          if (!$pkg->enable(false)) err('failed_to_enable_required_pkg', $pkg->id);
        }
      }
    }

    if ($this->setup('enable')) {
      $this->enabled = true;
      return pkg::list_add($this->id, pkg::LIST_WRITE);
    } else {
      return false;
    }
  }

  # disable package
  # $tree disable all package that are dependees of this pacake
  function disable(bool $tree=true): bool {
    if (!$this->enabled) err('already_disabled', $this->id);

    # disable packages tree
    if ($tree) {
      $dependees = $this->get_dependees();
      if (count($dependees)) {
        foreach ($dependees as $id) {
          $pkg = pkg::get($id);
          if ($pkg->enabled) {
            if (!$pkg->disable(false)) err('failed_to_disable_required_pkg', $pkg->id);
          }
        }
      }
    }

    if ($this->setup('disable')) {
      $this->enabled = false;
      return pkg::list_remove($this->id, pkg::LIST_WRITE);
    } else {
      return false;
    }
  }

  # execute setup for package
  # $action enable|disable
  function setup(string $action): bool {
    $id = $this->id;
    $setup = pkgpath($id, 'etc/setup.php');
    $class = "\\{$id}\\etc\\setup";

    # No setup file means success
    if (!file_exists($setup) || !method_exists($class, $action)) return true;

    try {
      // Execute setup method
      return call_user_func("{$class}::{$action}");
    } catch (\Exception $e) {
      return false;
    }
  }

  # list dependencies of package
  # $deep resolve deeper relationships i.e. dpeendencies of dependencies
  # $proc internal helper preventign infinite loops
  # return [ enabled: [], disabled: [], missing: [], version: [] ]
  function get_dependencies(bool $deep=false, array $proc=[]): array {
    $list = [
      'enabled'  => [],
      'disabled' => [],
      'missing'  => [],
      'version'  => []
    ];

    foreach ($this->require as $id => $req_version) {
      # php extension
      if (substr($id, 0, 14) === 'php.extension.') {
        $extension = substr($id, 14);
        if (extension_loaded($extension)) {
          $list['enabled'][] = $id;
        } else {
          $list['missing'][] = $id;
        }
        continue;
      }
      # normal package
      if (!pkg::exists($id)) {
        $list['missing'][] = $id;
      } else {
        $pkg = pkg::get($id);
        if (!pkg::version_match($req_version, $pkg->version)) {
          $list['version'][] = $id;
        } else if ($pkg->enabled) {
          $list['enabled'][] = $id;
        } else {
          $list['disabled'][] = $id;
        }
      }
    }

    # quite if deep list if not needed
    if (!$deep) return $list;

    # prevent infinite loops
    if (in_array($this->id, $proc)) err('infinite_loop_cross_dependencies', $proc);
    else $proc[] = $this->id;

    foreach ($list['disabled'] as $dependency) {
      $pkg = pkg::get($dependency);
      $nlist = $pkg->get_dependencies(true, $proc);

      $list['enabled']  = array_merge($nlist['enabled'],  $list['enabled']);
      $list['disabled'] = array_merge($nlist['disabled'], $list['disabled']);
      $list['missing']  = array_merge($nlist['missing'],  $list['missing']);
      $list['version']  = array_merge($nlist['version'],  $list['version']);
    }

    # eliminate duplicated entries
    $list['enabled']  = array_unique($list['enabled']);
    $list['disabled'] = array_unique($list['disabled']);
    $list['missing']  = array_unique($list['missing']);
    $list['version']  = array_unique($list['version']);

    return $list;
  }

  # list dependees - the packages which require provided package (ie are dependant on it)
  # $deep resolve deeper relationships i.e. dependees of dependees
  # $proc internal helper preventign infinite loops
  # return [ pkg_id, pkg_id ]
  function get_dependees(bool $deep=false, array $proc=[]): array
  {
    $packages = pkg::list();
    $list = [];

    foreach ($packages as $pkg) {
      if ($pkg->require && isset($pkg->require[$this->id])) {
        $list[] = $pkg->id;
      }
    }

    if (!$deep) return $list;

    # prevent infinite loops
    if (in_array($this->id, $proc)) err('infinite_loop_cross_dependencies', $proc);
    else $proc[] = $this->id;

    foreach ($list as $pkg) {
      $list = array_merge(
        $pkg->get_dependees(true, $proc),
        $list
      );
    }

    # eliminate duplicated entries
    return array_unique($list);
  }

  function __get(string $name) {
    if ($name === 'meta') return $this->meta;
    return array_key_exists($name, $this->meta) ? $this->meta[$name] : null;
  }

  function __set(string $name, $value) {
    if ($name === 'meta') { $this->meta = $value; }
    else { $this->meta[$name] = $value; }
  }
}