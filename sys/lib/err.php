<?php

namespace sys;

class err extends \Exception
{
  static function t($id, $message=null, $code=0)
  {
    $message = $message ? "{$id}: {$message}" : $id;
    $e = new self($message, $code);
    $e->id($id);
    throw $e;
  }

  static function throw($id, $message=null, $code=0)
  {
    self::t($id, $message, $code);
  }

  private $id;

  function __construct($message, $code=0, \Exception $previous=null)
  {
    parent::__construct($message, $code, $previous);
  }

  function id($id=null)
  {
    if (is_null($id)) return $this->id;
    else $this->id = $id;
  }
}
