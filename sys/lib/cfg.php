<?php

namespace sys;

use \sys\arr;

class cfg
{
  // Individual packages config
  private $pkg = [];

  // Application's config (dat/etc/cfg.ym)
  private $dat = [];

  // Application's rewrites (dat/etc/cfg.pkg.ym)
  private $rew = [];

  // Final merged config
  private $fin = [];

  function __construct()
  {
    list($fin, $dat, $rew) = $this->reload();
    $this->dat = $dat;
    $this->rew = $rew;
    $this->fin = arr::merge($fin, $dat, $rew);
  }

  function get_config($package, $file='cfg.ym'): array
  {
    $path = pkgpath($package, 'etc', $file);

    if (file_exists($path))
    {
      return ym::decode_file($path);
    }
    else
    {
      return [];
    }
  }

  function reload()
  {
    $fin = [];
    $dat = ['dat' => $this->get_config('dat')];

    $rew = $this->get_config('dat', 'cfg.pkg.ym');
    if (isset($_SERVER['SERVER_NAME'])) {
      $rew2 = $this->get_config('dat', 'cfg.pkg.'.$_SERVER['SERVER_NAME'].'.ym');
      $rew  = arr::merge($rew, $rew2);
    }

    pkg::map(function ($pkg) use (&$fin) {
      $c = $this->get_config($pkg->id);
      if (!empty($c)) {
        $this->pkg[$pkg->id] = $c;
        $fin = array_merge_recursive($fin, [$pkg->id => $c]);
      }
    }, pkg::LIST_ENABLED);

    return [$fin, $dat, $rew];
  }

  function get(string $key, $default = null)
  {
    return arr_path($this->fin, $key, $default);
  }

  function set(string $key, $value)
  {
    if (strpos($key, '.'))
    {
      $k = explode('.', $key, 2);
      $this->fin[$k[0]][$k[1]] = $value;
    }
    else $this->fin[$key] = $value;
  }

  function dump(): array
  {
    return [
      'pkg' => $this->pkg,
      'dat' => $this->dat,
      'rew' => $this->rew,
      'fin' => $this->fin
    ];
  }

  function apply(array $config, bool $merge = false)
  {
    $this->fin = $merge ? array_merge_recursive($this->fin, $config) : $config;
  }

  function override($id, $data)
  {
    # See what's different
    $fin = $this->fin;
    $rew = $this->rew;

    if (!isset($fin[$id]) || !is_array($fin[$id])) {
      return false;
    } else {
      $fin = $fin[$id];
    }

    $dat = isset($rew[$id]) ? $rew[$id] : [];

    foreach ($data as $d_id => $d_val)
    {
      if (!isset($fin[$d_id])) { continue; }

      if ($fin[$d_id] !== $d_val)
      {
        $dat[$d_id] = $d_val;
      }
      elseif (isset($dat[$d_id]) && $id !== 'dat')
      {
        unset($dat[$d_id]);
      }
    }

    if (empty($dat) && isset($rew[$id]))
    {
      unset($rew[$id]);
    }
    else
    {
      $rew[$id] = $dat;
    }

    list($fin, $_) = $this->reload();
    $this->rew = $rew;
    $this->fin = $this->apply($rew, true);
  }

  function write()
  {
    $filename = datpath('etc/cfg.pkg.ym');
    return ym::encode_file($filename, $this->rew);
  }
}
