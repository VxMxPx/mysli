<?php

namespace sys\cli;

use \sys\math;

class output
{

  # this is a list of all formats with codes
  # for details see: http://misc.flogisoft.com/bash/tip_colors_and_formatting
  private static $format =
  [
    # clean all
    "all" => [0, 0],

    # formatting
    "bold"      => [1, 0],
    "dim"       => [2, 0],
    "underline" => [4, 0],
    "blink"     => [5, 0],
    "invert"    => [7, 0], # invert the foreground and background colors
    "hidden"    => [8, 0],

    # foreground (text) colors
    "default"       => [39, 39],
    "black"         => [30, 39],
    "red"           => [31, 39],
    "green"         => [32, 39],
    "yellow"        => [33, 39],
    "blue"          => [34, 39],
    "magenta"       => [35, 39],
    "cyan"          => [36, 39],
    "light_gray"    => [37, 39],
    "dark_gray"     => [90, 39],
    "light_red"     => [91, 39],
    "light_green"   => [92, 39],
    "light_yellow"  => [93, 39],
    "light_blue"    => [94, 39],
    "light_magenta" => [95, 39],
    "light_cyan"    => [96, 39],
    "white"         => [97, 39],

    # background colors
    "bg_default"       => [49, 49],
    "bg_black"         => [40, 49],
    "bg_red"           => [41, 49],
    "bg_green"         => [42, 49],
    "bg_yellow"        => [43, 49],
    "bg_blue"          => [44, 49],
    "bg_magenta"       => [45, 49],
    "bg_cyan"          => [46, 49],
    "bg_light_gray"    => [47, 49],
    "bg_dark_gray"     => [100, 49],
    "bg_light_red"     => [101, 49],
    "bg_light_green"   => [102, 49],
    "bg_light_yellow"  => [103, 49],
    "bg_light_blue"    => [104, 49],
    "bg_light_magenta" => [105, 49],
    "bg_light_cyan"    => [106, 49],
    "bg_white"         => [107, 49],
  ];

  # length of last output line, this is used when aligning text to the right
  private static $last_length = 0;

  # when buffering rather than outputing
  private static $buffer = "";
  private static $buffering = false;

  /**
   * Print plain line of text.
   * --
   * @param string  $message
   * @param boolean $nl Append a new line at the end.
   */
  static function line($string, $nl=true)
  {
    static::$buffering
      ? (static::$buffer .= $string)
      : print($string);

    if ($nl)
    {
      static::$last_length = 0;
      static::$buffering
        ? (static::$buffer .= PHP_EOL)
        : print(PHP_EOL);
    }
    else
    {
      $clean = preg_replace("/\\e\[[0-9]+m/", "", $string);
      static::$last_length += strlen($clean);
    }
  }

  /**
   * Start buffering
   */
  static function buffer()
  {
    static::$buffering = true;
  }

  /**
   * Flush buffered data
   * --
   * @param boolean $return Return rather than output
   * --
   * @return string
   */
  static function flush($return=false)
  {
    static::$buffering = false;
    $buffer = static::$buffer;
    static::$buffer = "";

    if ($return) return $buffer;
    else static::line($buffer);
  }

  /**
   * Position input to the right part of the screen.
   * --
   * @param string  $message
   * @param integer $padding
   */
  static function right($message, $padding=2)
  {
    $len = strlen(preg_replace("/\\e\[[0-9]+m/", "", $message));
    $pos = util::terminal_width() - $padding - static::$last_length;
    $pos = $pos - $len;
    if ($pos < 0) $pos = 0;
    return str_repeat(" ", $pos) . $message;
  }

  /**
   * Format output.
   * --
   * @example
   * Use: <green>GREEN!</green>
   * <right>OK (Tags can be left open.)
   * --
   * @param string $format
   * @param array  $params Use vsprintf to modify output.
   */
  static function format($format, array $params=[])
  {
    $format = preg_replace_callback(
      "/<(\/)?([a-z_]{3,})>/i",
      function ($match)
      {
        if (isset(static::$format[$match[2]]))
        {
          $f = static::$format[$match[2]][(int) ($match[1] === "/")];
          return "\e[{$f}m";
        }
        elseif ($match[2] === "right")
        {
          return "[[[right/]]]";
        }
      },
      $format
    );

    # do we have new line character?
    if (substr($format, -1) === "\n")
    {
      $format = substr($format, 0, -1);
      $new_line = true;
    }
    else
    {
      $new_line = false;
    }

    $output = vsprintf($format, $params);

    if (strpos($format, "[[[right/]]]") !== false)
    {
      $output = explode("[[[right/]]]", $output, 2);
      static::line($output[0], false);
      static::line(static::right($output[1]), true);
    }
    else
    {
      static::line($output, $new_line);
    }
  }

  /**
   * Fill full width of the line with particular character(s).
   * --
   * @param string $character
   */
  static function fill($character)
  {
    $width = util::terminal_width() ?: 75;
    $width = floor($width / strlen($character));
    static::line(str_repeat($character, $width));
  }

  /**
   * Unordered list.
   * --
   * @param array   $list
   * @param integer $indent
   */
  static function ul(array $list, $indent=0)
  {
    foreach ($list as $line)
      is_array($line)
        ? static::ul($line, $indent+1)
        : static::indent("- {$line}", $indent);
  }

  /**
   * Ordered list.
   * --
   * @param array   $list
   * @param integer $indent
   */
  static function ol(array $list, $indent=0)
  {
    $i = 0;
    foreach ($list as $line)
      is_array($line)
        ? static::ol($line, $indent+1)
        : static::indent(++$i.". {$line}", $indent);
  }

  /**
   * Aligned list.
   * --
   * @param array   $list
   * @param integer $indent
   * @param integer $breakat Print new line at particular level (-1 disabled)
   */
  static function al(array $list, $indent=0, $breakat=-1)
  {
    $separator = " : ";
    $longest = 0;

    foreach ($list as $key => $val)
      $longest = strlen($key) > $longest
        ? strlen($key)
        : $longest;
    foreach ($list as $key => $value)
    {
      if ($breakat === $indent) { static::nl(); }

      if (is_array($value))
      {
        static::indent($key, $indent);
        static::al($value, $indent+1);
        continue;
      }
      elseif (is_bool($value))   $value = $value ? "true" : "false";
      elseif (is_string($value)) $value = "\"{$value}\"";

      static::indent(
        $key.str_repeat(" ", $longest - strlen($key)).$separator.$value,
        $indent);
    }
  }

  /**
   * Print line indented.
   * --
   * @param string  $string
   * @param integer $level
   */
  static function indent($string, $level=0)
  {
    static::line(str_repeat(" ", $level*2).$string);
  }


  /**
   * Make output more important.
   * --
   * @param string  $string
   * @param integer $indent
   */
  static function strong($string, $indent=0)
  {
    static::indent("\e[1m{$string}\e[0m", $indent);
  }

  /**
   * Print a general information to the user.
   * --
   * @param string $title
   * @param string $message
   *        Optional, if message is not provided,
   *        title will be used as a message.
   * @param integer $indent
   */
  static function info($title, $message=null, $indent=0)
  {
    $f = "\e[34m{$title}".($message ? ":\e[39m {$message}" : "\e[39m");
    static::indent($f, $indent);
  }

  /**
   * Print a warning.
   * --
   * @param string $title
   *
   * @param string $message
   *        Optional, if message is not provided,
   *        title will be used as a message.
   * @param integer $indent
   */
  static function warning($title, $message=null, $indent=0)
  {
    $f = "\e[33m{$title}".($message ? ":\e[39m {$message}" : "\e[39m");
    static::indent($f, $indent);
  }

  /**
   * Print an error.
   * --
   * @param string $title
   * @param string $message
   *        Optional, if message is not provided,
   *        title will be used as a message.
   * @param integer $indent
   */
  static function error($title, $message=null, $indent=0)
  {
    $f = "\e[31m{$title}".($message ? ":\e[39m {$message}" : "\e[39m");
    static::indent($f, $indent);
  }

  /**
   * Print an information that a job was successful.
   * --
   * @param string $title
   * @param string $message
   *        Optional, if message is not provided,
   *        title will be used as a message.
   * @param integer $indent
   */
  static function success($title, $message=null, $indent=0)
  {
    $f = "\e[32m{$title}".($message ? ":\e[39m {$message}" : "\e[39m");
    static::indent($f, $indent);
  }

  /**
   * Output a progress bar.
   * --
   * @param integer $current Current position
   * @param integer $max     Maximum position
   * @param string  $title   Progress bar title
   * @param array   $markers Markers [ filled, empty ]
   */
  static function progress($current, $max, $title, $markers=["#", " "])
  {
    $percent = (int) floor(math::get_percent($current, $max));
    $paint   = (int) floor( $percent / 2 );

    $bar = "";
    if ($paint > 0)  $bar .= str_repeat($markers[0], $paint);
    if ($paint < 50) $bar .= str_repeat($markers[1], 50-$paint);

    $title = $title ? "{$title} " : "";

    static::progress_f("%s[%s] (%s%%)", [$title, $bar, $percent]);
  }

  /**
   * Output formatted progress bar.
   * --
   * @param string $format
   * @param array  $variables
   */
  static function progress_f($format, array $variables)
  {
    static::line(vsprintf($format, $variables)."\r", false);
  }

  /**
   * Insert an empty line(s).
   * --
   * @param integer $num number of new lines
   */
  static function nl($num=1)
  {
    static::line(str_repeat(PHP_EOL, ((int) $num)-1));
  }
}
