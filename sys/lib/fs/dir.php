<?php

namespace sys\fs;

use \sys\log;

class dir
{
  /**
    * Create a new directory.
    * --
    * @param string  $directory
    * @param integer $mode
    * @param boolean $recursive
    * --
    * @return boolean
    */
  static function create($directory, $mode=0777, $recursive=true)
  {
    if (dir::exists($directory))
    {
      return true;
    }

    if (file::exists($directory))
    {
      err('already_exists', $directory);
    }

    log::info("Create: `{$directory}`, recursive: {$recursive}", __CLASS__);

    return mkdir($directory, $mode, $recursive);
  }

  /**
    * Return directory name, without filename.
    * --
    * @param string $dir
    * --
    * @return string
    */
  static function name($dir)
  {
    return dirname($dir);
  }

  /**
    * Copy a directory and all the content to the destination.
    * If destination doesn't exists, it will be created.
    * --
    * @param  string  $source
    * @param  string  $destination
    * @param  boolean $recursive   Copy sub-directories.
    * @param  boolean $overwrite   If destination (file!) exists overwrite it.
    * --
    * @return integer Number of copied files and directories.
    */
  static function copy($source, $destination, $recursive=true, $overwrite=true)
  {
    $count = 0; // number of copied files and directories

    if (!static::exists($source))
    {
      err('not_a_valid_directory', $source);
    }

    if (!static::exists($destination))
    {
      if (!static::create($destination))
      {
        err('cannot_create_destination', $destination);
      }
      else
      {
        $count++;
      }
    }

    $files = array_diff(scandir($source), ['.', '..']);

    foreach ($files as $file)
    {
      $filename = ds($source, $file);

      if (static::exists($filename))
      {
        if ($recursive)
        {
          $count += static::copy(
            $filename, ds($destination, $file), $recursive, $overwrite);
        }
      }
      else
      {
        try
        {
          log::info(
            "Copy: `{$filename}` to directory: `{$destination}` ".
            "as: `{$file}`, overwrite: `{$overwrite}`.",
            __CLASS__);
          $count += file::copy($filename, ds($destination, $file), $overwrite);
        }
        catch (\Exception $e)
        {
          log::warning(
            'Copying failed, with message: `{message}`.',
            [__CLASS__, 'exception' => $e]);
        }
      }
    }

    return $count;
  }

  /**
    * Move directory and all the content to the destination.
    * If destination doesn't exists, it will be created.
    * --
    * @param  string  $source
    * @param  string  $destination
    * @param  boolean $overwrite   If destination (file!) exists overwrite it.
    * --
    * @return integer Number of moved files and directories.
    */
  static function move($source, $destination, $overwrite=true)
  {
    $count = 0; // number of moved files and directories

    if (!static::exists($source))
    {
      err('not_a_valid_directory', $source);
    }

    if (!static::exists($destination))
    {
      if (!static::create($destination))
      {
        err('cannot_create_destination_directory', $destination);
      }
      else
      {
        $count++;
      }
    }

    $files = array_diff(scandir($source), ['.', '..']);

    foreach ($files as $file)
    {
      $filename = ds($source, $file);

      if (static::exists($filename))
      {
        $count += static::move($filename, ds($destination, $file), $overwrite);
      }
      else
      {
        try
        {
          log::info(
            "Move: `{$filename}` to directory: `{$destination}` ".
            "as: `{$file}`, overwrite: `{$overwrite}`.",
            __CLASS__);
          $count += file::move($filename, ds($destination, $file), $overwrite);
        }
        catch (\Exception $e)
        {
          log::warning(
            'Moving failed, with message: `{message}`.',
            [__CLASS__, 'exception' => $e]);
        }
      }
    }

    try
    {
      static::remove($source, false);
    }
    catch (\Exception $e)
    {
      log::warning(
        'Failed to remove source `{$source}`: `{message}`.',
        [__CLASS__, 'exception' => $e]);
    }

    return $count;
  }

  /**
    * Removed a directory.
    * --
    * @param string $directory
    *        Need to exists, cannot be empty string and cannot be root '/'.
    *
    * @param boolean $force
    *        Remove non empty directory.
    * --
    * @return boolean
    */
  static function remove($directory, $force=true)
  {
    if (!$directory || empty($directory) || trim($directory) === '/')
    {
      err('invalid_directory', $directory);
    }

    if (!static::exists($directory))
    {
      log::warn(
        "Cannot remove, directory doesn't exists: `{$directory}`.", __CLASS__);
      return true;
    }

    if (!static::is_empty($directory))
    {
      if (!$force)
      {
        err('directory_not_empty', $directory);
      }
      $files = array_diff(scandir($directory), ['.','..']);

      foreach ($files as $file)
      {
        $filename = ds($directory, $file);

        if (static::exists($filename))
        {
          static::remove($filename, $force);
          continue;
        }

        if (!unlink($filename))
        {
          err('could_not_remove_file', $filename);
        }
      }
    }

    if (!rmdir($directory))
    {
      err('could_not_remove_directory', $directory);
    }
    else
    {
      return true;
    }
  }

  /**
    * Get signatures of all files in the directory +
    * sub directories if $deep is true.
    * --
    * @param string  $directory
    * @param boolean $deep
    * @param boolean $ignore_hidden Ignore hidden files and folders.
    * --
    * @return array
    */
  static function signature($directory, $deep=true, $ignore_hidden=true)
  {
    if (!static::exists($directory))
    {
      err('directory_doesnt_exists', $directory);
    }

    $result = [];
    $files = array_diff(scandir($directory), ['.','..']);

    foreach ($files as $file)
    {
      if ($ignore_hidden && substr($file, 0, 1) === '.')
      {
        continue;
      }

      $filename = ds($directory, $file);

      if (static::exists($filename))
      {
        $result = array_merge($result, static::signature($filename));
      }
      else
      {
        $result[$filename] = file::signature($filename);
      }
    }

    return $result;
  }

  /**
    * Check weather directory is readable.
    * --
    * @param string $directory
    * --
    * @return boolean
    */
  static function is_readable($directory)
  {
    return is_readable($directory);
  }

  /**
    * Check if there are files in the directory.
    * --
    * @param string $directory
    * --
    * @return boolean
    */
  static function is_empty($directory)
  {
    if (!static::is_readable($directory))
    {
      err('directory_is_not_readable', $directory);
    }

    $handle = opendir($directory);

    while (false !== ($entry = readdir($handle)))
    {
      if ($entry !== '.' && $entry !== '..')
      {
        return false;
      }
    }

    return true;
  }

  /**
    * Return directory size in bytes.
    * --
    * @param string  $directory
    * @param boolean $deep      Weather to include sub-directories.
    * --
    * @return integer
    */
  static function size($directory, $deep=true)
  {
    if (!static::exists($directory))
    {
      err('directory_not_found', $directory);
    }

    $files = array_diff(scandir($directory), ['.','..']);
    $size = 0;

    foreach ($files as $file)
    {
      if (static::exists(ds($directory, $file)))
      {
        if ($deep)
        {
          $size += static::size(ds($directory, $file));
        }
      }
      else
      {
        $size += file::size(ds($directory, $file));
      }
    }

    return $size;
  }

  /**
    * Check if directory exists.
    * --
    * @param string $directory
    * --
    * @return boolean
    */
  static function exists($directory)
  {
    return file_exists($directory) && is_dir($directory);
  }

  /**
   * Map segments up from current.
   * /my/dir/one =>
   *  1. dir: my/dir cut: one
   *  2. dir: my cut: dir/one
   * --
   * @param string $dir
   * @param function $callback function($dir, $cut) {}
   */
  static function map_up($dir, $callback)
  {
    $dir = ds($dir);
    $len = strlen($dir);
    $position = $len;
    $first = strpos($dir, '/');

    if ($first === false) {
      return;
    }

    while ($position > $first) {
      $position = strrpos($dir, '/', $position-($len+1));
      $callback(substr($dir, 0, $position+1), substr($dir, $position+1));
    }
  }
}
