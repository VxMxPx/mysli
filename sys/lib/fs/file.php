<?php

namespace sys\fs;

use \sys\log;

class file
{
  const prepend = 0;
  const append = 1;
  const replace = 2;

  /**
   * Return only extension of file if available otherwise null.
   * Example:
   *   ui.min.js => .js
   * --
   * @param string  $path
   * @param boolean $invert  Return only file ratherthan extension.
   * --
   * @return string
   */
  static function extension($path, $invert=false)
  {
    $file = basename($path);
    $extension = strpos($file, '.') ? substr($file, strrpos($file, '.')) : null;
    if ($invert)
    {
      return substr($path, 0, -strlen($extension));
    }
    else
    {
      return $extension;
    }
  }

  /**
   * Get filename.
   * --
   * @param string  $filename
   * @param boolean $extension Include file extension?
   * --
   * @return string
   */
  static function name($filename, $extension=true)
  {
    $filename = basename($filename);

    if (!$extension)
    {
      $ext = static::extension($filename);
      if ($ext)
      {
        return substr($filename, 0, -(strlen($ext)));
      }
    }

    return $filename;
  }

  /**
   * Return file path (no filename).
   * --
   * @param string $filename
   * --
   * @return string
   */
  static function path($filename)
  {
    return dirname($filename);
  }

  /**
   * Return file size in bytes. File must exists.
   * --
   * @param string $filename
   * --
   * @return integer
   */
  static function size($filename)
  {
    if (!static::exists($filename))
    {
      err('file_not_found', $filename);
    }
    return filesize($filename);
  }

  /**
   * file_exists wrapper
   * --
   * @param string $filename
   * --
   * @return boolean
   */
  static function exists($filename)
  {
    return file_exists($filename);
  }

  /**
   * Get file's content if file exists.
   * --
   * @param string $filename
   * --
   * @return string
   */
  static function read($filename)
  {
    if (static::exists($filename))
    {
      return file_get_contents($filename);
    }
    else
    {
      err('file_not_found', $filename);
    }
  }

  /**
   * Create a new file, if doesn't exists already.
   * If it does exists (and $empty = true) it will remove existing content.
   * --
   * @param string  $filename
   * @param boolean $empty
   * --
   * @return boolean
   */
  static function create($filename, $empty=false)
  {
    if (static::exists($filename))
    {
      if (!$empty)
      {
        return false;
      }

      if (file_put_contents($filename, '') === false)
      {
        err('couldnt_remove_files_contents', $filename);
      }
    }

    log::info("Create: `{$filename}`", __CLASS__);

    return touch($filename);
  }

  /**
   * Create a new file, if doesn't exists already.
   * This can create directory also, if is not there already.
   * --
   * @param string  $filename
   * @param boolean $empty
   * --
   * @return boolean
   */
  static function create_recursive($filename, $empty=false)
  {
    $dir = dirname($filename);

    if (!dir::exists($dir))
    {
      dir::create($dir, 0777, true);
    }

    return static::create($filename, $empty);
  }

  /**
   * Write a content to the file.
   * --
   * @param  string  $filename Full absolute path.
   * @param  string  $content
   * @param  integer $method   file::append, file::prepend, file::replace
   * @param  boolean $create
   * --
   * @return integer Number of bytes written.
   */
  static function write(
    $filename, $content, $method=self::replace, $lock=false, $create=true)
  {
    if (!static::exists($filename) && $create)
    {
      static::create($filename);
    }

    if (!static::exists($filename))
    {
      err('file_doesnt_exists', $filename);
    }

    log::info(
      "Writting to file: `{$filename}`, method: `{$method}`, lock: `{$lock}`.",
      __CLASS__);

    if ($method === self::prepend)
    {
      $handle = fopen($filename, 'r+t');

      if ($handle === false)
      {
        err('couldnt_open_file', $filename);
      }

      if ($lock)
      {
        if (!flock($handle, LOCK_EX))
        {
          err('couldnt_lock_the_file', $filename);
        }
      }

      $content_length = strlen($content);
      $sum_length = filesize($filename) + $content_length;
      $content_old = fread($handle, $content_length);
      rewind($handle);
      $i = 1;

      while (ftell($handle) < $sum_length)
      {
        fwrite($handle, $content);
        $content = $content_old;
        $content_old = fread($handle, $content_length);
        fseek($handle, $i * $content_length);
        $i++;
      }

      fflush($handle);

      if ($lock)
      {
        flock($handle, LOCK_UN);
      }

      fclose($handle);
      return $i;
    }
    else
    {
      if ($method === self::append)
      {
        $flags = FILE_APPEND;
      }
      else
      {
        $flags = 0;
      }

      if ($lock)
      {
        $flags = $flags|LOCK_EX;
      }

      $r = file_put_contents($filename, $content, $flags);

      if ($r === false)
      {
        err('couldnt_write_content_to_file', $filename);
      }

      return $r;
    }
  }

  /**
   * Remove one (or more files).
   * --
   * @param mixed $file String or an array to remove more than one file.
   * --
   * @return integer Number of removed files.
   */
  static function remove($file)
  {
    if (is_array($file))
    {
      $i = 0;

      foreach ($file as $f)
      {
        $i = $i + static::remove($f);
      }

      return $i;
    }

    log::info("Remove: `{$file}`", __CLASS__);

    return unlink($file) ? 1 : 0;
  }

  /**
   * Copy file from source to destination.
   * --
   * @param mixed   $source      Absolute path.
   * @param string  $destination Absolute path.
   * @param boolean $overwrite   If destination exists, overwrite it.
   * --
   * @return boolean
   */
  static function copy($source, $destination, $overwrite=true)
  {
    if (dir::exists($destination))
    {
      $destination = ds($destination, '/', static::name($source));
    }
    else
    {
      if (!dir::exists(dirname($destination)))
      {
        err('destination_directory_not_found', $destination);
      }
    }

    if (static::exists($destination) && !$overwrite)
    {
      err('destination_file_exists', $destination);
    }

    log::info(
      "Copy: `{$source}` to `{$destination}`, overwrite: `{$overwrite}`.",
      __CLASS__);

    return copy($source, $destination);
  }

  /**
   * Move a file from source to destination.
   * --
   * @param mixed   $source      Absolute path.
   * @param string  $destination Absolute path.
   * @param boolean $overwrite   If destination exists, overwrite it.
   * --
   * @return boolean
   */
  static function move($source, $destination, $overwrite=true)
  {
    if (dir::exists($destination))
    {
      $destination = ds($destination, '/', static::name($source));
    }
    else
    {
      if (!dir::exists(dirname($destination)))
      {
        err('destination_directory_not_found', $destination);
      }
    }

    if (static::exists($destination) && !$overwrite)
    {
      err('destination_file_exists', $destination);
    }

    log::info(
      "Move: `{$source}` to `{$destination}`, overwrite: `{$overwrite}`.",
      __CLASS__);

    return move($source, $destination);
  }

  /**
   * Rename a file.
   * --
   * @param mixed  $source      Absolute path.
   * @param string $destination Absolute path.
   * --
   * @return boolean
   */
  static function rename($source, $destination)
  {
    if (strpos($destination, '/') === false &&
      strpos($destination, '\\') === false)
    {
      $destination = ds(dirname($source), $destination);
    }

    if (dirname($source) !== dirname($destination))
    {
      err(
        'destination_and_source_dir_must_equal',
        "d({$destination}) s({$source})");
    }

    if (basename($source) === basename($destination)) {
      err(
        'destination_and_source_file_must_differ',
        "d({$destination}) s({$source})");
    }

    log::info("Rename: `{$source}` to `{$destination}`.", __CLASS__);

    return \rename($source, $destination);
  }

  /**
   * Find files in particular directory.
   * --
   * @param string $directory
   *
   * @param string $filter
   *        Regular expression filter, e.g. `/.*\.jpg/i`.
   *        Simple filter is supported. (@see fs::filter_to_regex())
   *
   * @param boolean $deep
   *        Include sub-directories.
   *
   * @param integer $mrel
   *        Relative path (cut off root directory segment)
   *        will require whole segment to match.
   *
   * @param integer $rootlen
   *        Use internally for length of a root, you can
   *        pass a value, to set how much of the file path should be removed,
   *        default is `strlen($directory)`.
   * --
   * @return array
   */
  static function find(
    $directory, $filter=null, $deep=true, $mrel=false, $rootlen=null)
  {
    if (!dir::exists($directory))
    {
      err('not_a_valid_directory', $directory);
    }

    log::info("Searching: `{$directory}`, filter: `{$filter}`.", __CLASS__);

    // Grab files in the selected directory...
    $collection = array_diff(scandir($directory), ['.','..']);
    $matched    = [];

    // No files were found at all
    if (empty($collection))
    {
      return [];
    }

    // Convert simple filter to the regular expression
    if ($filter && substr($filter, 0, 1) !== '/')
    {
      $filter = fs::filter_to_regex($filter);
    }


    // If there's no $rootlen, it will be acquired from the directory.
    // This is used latter in recursion.
    $rootlen = $rootlen ?: strlen(rtrim($directory, '\\/'))+1;

    /*
    Start looking for files.
     */
    foreach ($collection as $file)
    {
      if (dir::exists(ds($directory, $file)))
      {
        if (!$deep) continue;

        $matched_sub = static::find(
          ds($directory, $file), $filter, $deep, $mrel, $rootlen);
        $matched = array_merge($matched, $matched_sub);
        continue;
      }

      // Full file path
      $ffile  = ds($directory, $file);
      // Relative file path
      $rfile = substr($ffile, $rootlen);

      // Match either to relative, or filename itself
      if ($filter && !preg_match($filter, ($mrel ? $rfile : $file)))
      {
        continue;
      }

      $matched[$rfile] = $ffile;
    }

    return $matched;
  }

  /**
   * Return a md5 signature of specified file(s).
   * --
   * @param  mixed $filename
   *         Filename (full path), or an array, collection of files,
   *         e.g. `['/abs/path/file.1', '/abs/path/file.2']`.
   * --
   * @return mixed string | array, depends on the input.
   */
  static function signature($filename)
  {
    if (is_array($filename))
    {
      $collection = [];

      foreach ($filename as $file)
      {
        $collection[$file] = static::signature($file);
      }

      return $collection;
    }

    if (!file::exists($filename))
    {
      err('file_not_found', $filename);
    }

    return md5_file($filename);
  }
}
