<?php

namespace sys; class event
{

static $list = [];

# Fire an event.
# @param    $event
# @param ...$args  - Parameters to be provided to the called function
static function fire(string $event, ...$args)
{
  if (!isset(static::$list[$event])) return;
  if (is_callable('log::add')) log::info("Fire: `{$event}`.", __CLASS__);
  foreach (static::$list[$event] as $call) call_user_func_array($call, $args);
}

# Fire and event and modify variable.
# @param $event
# @param $variable - To be passed by a reference
static function modify(string $event, &$variable)
{
  if (!isset(static::$list[$event])) return;
  if (is_callable('log::add')) log::info("Fire: `{$event}`.", __CLASS__);
  foreach (static::$list[$event] as $call) $call($variable);
}

# Wait for an event to be tirggered.
# @param $event - call@event.event | call@event.event#id
# @param $call
static function on(string $event, callable $call)
{
  $seg = explode('#', $event, 2);

  if (isset($seg[1])) $id = $seg[1]; else $id = null;
  $event = $seg[0];

  if (!isset(static::$list[$event])) static::$list[$event] = [];

  if ($id) static::$list[$event][$id] = $call;
  else static::$list[$event][] = $call;
}

# Stop waiting for an event to be triggered.
# @param $event - call@event.event#id | #id
static function off(string $event)
{
  if (strpos($event, '#') === 0) {
    $id = substr($event, 1);
    foreach (static::$list as $event => $events)
    {
      foreach ($events as $sid => $_)
      {
        if ($sid === $id)
        {
          unset(static::$list[$event][$sid]);
        }
      }
    }
  }
  elseif (strpos($event, '#'))
  {
    list($event, $id) = explode('#', $event, 2);
    if (!isset(static::$list[$event])) return;
    if (isset(static::$list[$event][$id])) unset(static::$list[$event][$id]);
  }
  else
  {
    static::$list[$event] = [];
  }
}

}
