<?php

namespace sys; class ini
{
  static function decode($string)
  {
    return parse_ini_string($string, true, INI_SCANNER_TYPED);
  }

  static function decode_file($file)
  {
    return parse_ini_file($file, true, INI_SCANNER_TYPED);
  }

  static function encode(array $in, $categories=true)
  {
    $buffer = '';

    # add non-array values
    foreach ($in as $key => $val)
    {
      if (is_array($val) && $categories) {
        continue;
      }
      else
      {
        $val = self::convert_value($val);
        $buffer .= "{$key}={$val}\n";
      }
    }

    # add array values
    if ($categories) {
      foreach ($in as $key => $val)
      {
        if (is_array($val)) {
          $buffer .= "\n[{$key}]\n";
          $buffer .= self::encode($val, false);
          $buffer .= "\n";
        }
      }
    }

    return $buffer;
  }

  static function encode_file($filename, array $in, bool $categories=true)
  {
    return file_put_contents($filename, self::encode($in, $categories)) !== false
      ? !logi("ini_encode_to_file:${filename}", __CLASS__)
      : logw("ini_encode_to_file:${filename}", __CLASS__);
  }

  private static function convert_value($value)
  {
    $keywords = ['yes', 'no', 'true', 'false', 'null'];
    $characters = ["\n", ';', '=', '\\', '!', "'"];
    switch (gettype($value))
    {
      case 'string':
        if (is_numeric($value))
        {
          return '"'.$value.'"';
        }
        elseif (in_array(strtolower($value), $keywords))
        {
          return '"'.$value.'"';
        }
        else
        {
          # Escape "
          $value = preg_replace('/(?<!\\\\)"/', '\"', $value);

          # If space in front/back quote it
          if (substr($value, 0, 1) === ' ' || substr($value, -1) === ' ')
          {
            return '"'.$value.'"';
          }
          else
          {
            foreach ($characters as $char)
              if (strpos($value, $char) !== false) return '"'.$value.'"';
          }

          # Not one of the characters, return value itself...
          return $value;
        }
      case 'double':
      case 'integer':
        return $value;
      case 'boolean':
        return $value ? 'true' : 'false';
      case 'array':
        return implode(',', $value);
      default:
        return "null";
    }
  }
}
