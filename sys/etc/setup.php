<?php namespace sys\etc;

class setup
{
  # data directories list
  static private $datpath_dir = ['autoload', 'etc', 'i18n', 'lib', 'tmp'];

  # index.php and m files
  # {{TYPE}} = index.php (INDEX), m (CLI)
  static private $index_template = <<<'INDEX'
<?php

call_user_func(function() {

  # define run type: CLI|INDEX
  define('MYSLI_RUN_TYPE', '{{TYPE}}');

  # define base paths for INDEX
  define('MYSLI_{{TYPE}}', __FILE__);
  define('MYSLI_{{TYPE}}_DIR', __DIR__);
  $ds = DIRECTORY_SEPARATOR;

  # define an error shortcut
  function terr($m) { trigger_error($m, E_USER_ERROR); }

  # check for, and include config
  if (!file_exists(MYSLI_{{TYPE}}_DIR.$ds.'config.php')) terr("File not found: `{$config}`.");
  include MYSLI_{{TYPE}}_DIR.$ds.'config.php';

  # make sure base constants are defined
  foreach(['PUBPATH', 'PKGPATH', 'DATPATH', 'PKG_SYS'] as $req)
    defined("MYSLI_{$req}") or terr("Undefined: `MYSLI_{$req}`, check `config.php`.");

  # include and run toolkit package
  $sys_file = MYSLI_PKGPATH.$ds.MYSLI_PKG_SYS.$ds."autoload{$ds}init.php";
  if (!file_exists($sys_file)) terr('Failed to initialize toolkit: '.MYSLI_PKG_SYS);
  else include $sys_file;

  # shutdown
  r('sys')->shutdown();
});
INDEX;

  # Config
  static private $config_template = <<<'CONFIG'
<?php

# Define paths
define('MYSLI_PUBPATH', __DIR__);
define('MYSLI_PKGPATH', '{{PKGPATH}}');
define('MYSLI_DATPATH', '{{DATPATH}}');

# Expected format `pkg` will look for: pkg/autoload/init (no .php)
define('MYSLI_PKG_SYS', 'sys');

# Print log to command line
define('MYSLI_LOG_TO_CLI', 0);
CONFIG;

  static function enable()
  {
    $ds = DIRECTORY_SEPARATOR;

    # Create base directories
    if (!is_dir(MYSLI_PKGPATH)) { mkdir(MYSLI_PKGPATH, 0777, true); }
    if (!is_dir(MYSLI_PUBPATH)) { mkdir(MYSLI_PUBPATH, 0777, true); }
    if (!is_dir(MYSLI_DATPATH)) { mkdir(MYSLI_DATPATH, 0777, true); }

    # Create other directories
    foreach (static::$datpath_dir as $path) {
      mkdir(MYSLI_DATPATH."/{$path}", 0777, true);
    }

    # write m and index
    $result =
    file_put_contents(MYSLI_PUBPATH.'/index.php', str_replace('{{TYPE}}', 'INDEX', static::$index_template))
    && file_put_contents(MYSLI_PUBPATH.'/m', "#!/bin/php\n".str_replace('{{TYPE}}', 'CLI', static::$index_template))
    && file_put_contents(MYSLI_PUBPATH.'/config.php', str_replace(
      ['{{PKGPATH}}', '{{DATPATH}}'],
      [MYSLI_PKGPATH, MYSLI_DATPATH],
      static::$config_template));

    if (!$result) {
      throw new \Exception("Could not write template file.", 1);
    }

    # create packages file, read sys/pkg.ini
    $packagesf = MYSLI_DATPATH."{$ds}etc{$ds}packages";
    $sysini = MYSLI_PKGPATH."{$ds}sys{$ds}pkg.ini";
    if (!file_exists($packagesf))
    {
      if (!file_exists($sysini)) {
        throw new \Exception("System pkg.ini file not found: `{$sysini}`", 2);
      }
      $meta = file_get_contents($sysini);
      $meta = parse_ini_string($meta, true, INI_SCANNER_TYPED);
      $version = $meta['version'];
      $date = gmdate('c');
      if (!file_put_contents($packagesf, "# package;version;date\nsys;{$version};{$date}\n")) {
        throw new \Exception("Cannot write pkg file: `{$packagesf}`", 3);
      }
    }

    # create empty config files
    $result =
    file_put_contents(MYSLI_DATPATH.'/etc/cfg.pkg.ym', '# packages configurations overrides')
    && file_put_contents(MYSLI_DATPATH.'/etc/cfg.pkg.ym', '# application unique (dat) configurations');

    if (!$result) {
      throw new \Exception("Could not write config files.", 2);
    }

    return true;
  }
}
