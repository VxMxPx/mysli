<?php

namespace sys\command;

use \sys\cli\output as out;
use \sys\pkg as libpkg;

class pkg
{
  function run(array $args)
  {
    $cmd = array_shift($args);

    switch ($cmd)
    {
      case '-e':
      case '--enable':
        return $this->enable($args[0]);

      case '-d':
      case '--disable':
        return $this->disable($args[0]);

      case '-r':
      case '--reload':
        return $this->reload($args[0]);

      case '--details':
        return $this->details($args[0]);

      case '-l':
      case '--list':
        return $this->list();

      case '-h':
      case '--help':
        return $this->help();

      default:
        out::error('Invalid command. Use: pkg --help');
    }
  }

  function reload($id)
  {
    $this->disable($id);
    return $this->enable($id);
  }

  function enable($id)
  {
    $pkg = libpkg::get($id);

    if ($pkg->enabled) {
      out::warning("Package is already enabled: `{$pkg->id}`.");
      return 0;
    }

    try
    {
      $pkg->enable()
        ? out::success("Successfully enabled: $id")
        : err('Unknown error.');
      return 0;
    }
    catch (\Exception $e)
    {
      out::error("Failed to enable!");
      out::error($e->getMessage());
      return 1;
    }
  }

  function disable($id)
  {
    $pkg = libpkg::get($id);

    if (!$pkg->enabled)
    {
      out::warning("Package is already disabled: `{$id}`.");
      return 0;
    }

    try
    {
      $pkg->disable()
        ? out::success("Successfully disabled: $id")
        : err('Unknown error.');
      return 0;
    }
    catch (\Exception $e)
    {
      out::error("Failed to disable!");
      out::error($e->getMessage());
      return 1;
    }
  }

  function details($id)
  {
    $pkg = libpkg::get($id);
    if ($pkg)
    {
      out::al($pkg->meta);
    }
    else
    {
      out::error("No such package: `{$id}`");
    }
  }

  function list()
  {
    $packages = libpkg::list();
    $enabled = [];
    $disabled = [];

    foreach ($packages as $pkg)
    {
      if ($pkg->enabled) $enabled[] = $pkg->id;
      else $disabled[] = $pkg->id;
    }

    out::nl();
    out::strong('Enabled:');
    out::ul($enabled);
    out::nl();
    out::strong('Disabled:');
    out::ul($disabled);

    return 0;
  }

  function help()
  {
    echo "Package Utility\n\n";
    echo "Usage: ./m pkg [OPTIONS]...\n\n";
    echo "Options:\n";
    echo "  -e, --enable  PACKAGE  Enable a package.\n";
    echo "  -d, --disable PACKAGE  Disable a package.\n";
    echo "  -r, --reload  PACKAGE  Disable, then enable package.\n";
    echo "  --details     PACKAGE  Get package's details.\n";
    echo "  -l, --list             Get list of packages.\n\n";
    echo "Other:\n";
    echo "  -h, --help  Display this help.\n";
    return 0;
  }
}
