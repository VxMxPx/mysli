<?php

namespace sys\command;

use \sys\cli\output as out;
use \sys\cli\input as cin;

class interactive
{
  # run the interactive mode
  function run(array $args)
  {
    $namespace = 'namespace sys\command\interactive\internal;';
    $multiline = false;
    $buffer = [];

    out::strong('Mysli Interactive Console');

    # see if there's code to be executed...
    $exec = array_search('--exec', $args);

    if (isset($args[$exec+1]))
    {
      $execute = $args[$exec+1];
      out::line('Execute: '.$execute);
      out::line(eval(
        $namespace."\n".'echo dump_r(' . trim($execute, ';') . ');'));
    }

    # now wait for the user input
    cin::line('>> ',
      function ($stdin) use ($namespace, &$multiline, &$buffer)
      {
        if ($multiline) out::line('... ', false);
        if (in_array(strtolower($stdin), ['exit', 'q'])) return true;

        if (in_array($stdin, ['help', 'h', '?']))
        {
          static::help();
          return;
        }

        if ($stdin === 'START')
        {
          $multiline = true;
          $buffer = [$namespace];
          out::info('Multiline input set.');
          out::line('... ', false);
          return;
        }

        if ($stdin === 'END')
        {
          if ($multiline)
          {
            $multiline = false;
            out::line(eval(implode("\n", $buffer)));
            return;
          }
          else
          {
            out::warning(
              'WARNING',
              "Cannot END, you must START multiline input first.");
            return;
          }
        }

        if (!$multiline)
        {
          if (substr($stdin, 0, 1) === '.')
          {
            out::line(eval(substr($stdin, 1)));
          }
          else
          {
            out::line(eval(
              $namespace."\n".
              'echo dump_r(' . trim($stdin, ';') . ');'));
          }
        }
        else
        {
          $buffer[] = $stdin;
        }
      }
    );
  }

  # display help message
  private function help()
  {
    out::strong("Mysli CLI Interactive\n");
    out::line(<<<HELP
Normal PHP commands are accepted, example: `\$var = 'value'`.
You don't need to enter `echo` and you can omit semicolons.
If you want command not to be echoed automatically, prefix it with dot `.`.
Multiline input is allowed, type: `START` to start it, and `END` to execute lines.
Enter `q` or `exit` to quit.

Use --exec CODE to execute code on start.
HELP
    );
  }
}
