<?php namespace sys\command;

use \sys\cli\output as out;

class cfg
{
  function run(array $args)
  {
    $cmd = array_shift($args);

    switch($cms)
    {
      // case '-l':
      // case '--list':
      default:
        $this->list();
    }
  }

  function list()
  {
    $cfg = r('cfg');
    out::nl();
    out::al($cfg->dump());
    return 0;
  }
}