<?php

namespace http; class router
{

  private $routes = [];
  private $request;

  function __construct(request $r)
  {
    $this->request = $r;
  }

  function any   (string $route, callable $call) {
    return $this->on('*',      $route, $call); }
  function get   (string $route, callable $call) {
    return $this->on('GET',    $route, $call); }
  function put   (string $route, callable $call) {
    return $this->on('PUT',    $route, $call); }
  function post  (string $route, callable $call) {
    return $this->on('POST',   $route, $call); }
  function delete(string $route, callable $call) {
    return $this->on('DELETE', $route, $call); }

  # register new route call
  # $method *|GET|POST|PUT|DELETE
  # $route /foo/bar | /foo/:param | /foo/...param | /foo/:param?
  # $call to be called on match function ($param, $param, array $params) {}
  function on(string $method, string $route, callable $call): \http\route
  {
    $route = new route($method, $route, $call);;
    $this->routes[] = $route;
    return $route;
  }

  # trigger router, scan all routes and call registered functions
  function fire()
  {
    $r = $this->request;
    # more than one registered route might be matched
    # in such case the one what's most strict will be assigned
    # (ie the one with most segments:
    # /...params vs /api/...params, the latter would won)
    $matched = [];

    foreach ($this->routes as $route)
    {
      if ($route->match($r->method(), $r->path()))
      {
        $matched[$route->weight()] = $route;
      }
    }

    # if there are any matched routes,
    # the one with most segments will be called and other will be ignored
    if (count($matched))
    {
      ksort($matched);
      $route = array_pop($matched);
      $route->call($r);
    }
  }

}
