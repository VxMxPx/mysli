<?php

namespace http;

use \sys\event;

class index
{
  static function run(): bool
  {
    event::fire('before.init@http.index');
dump('Hello');
    # Init base libraries for http
    r('request',  new request($_SERVER, $_POST, $_GET));
    r('response', new response());
    r('router',   new router(r('request')));

    event::fire('after.init@http.index');
    return true;
  }
}