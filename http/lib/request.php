<?php

namespace http;

use \sys\log;

class request
{
  private $segments;
  private $server;
  private $post;
  private $get;

  function __construct(array $server, array $post, array $get)
  {
    $this->server = $server;
    $this->post = $post;
    $this->get = $get;

    # dealing with application/json POST|PUT|DELETE
    if (empty($this->post)
    && ($this->method() === 'POST' || $this->method() === 'PUT' || $this->method() === 'DELETE')
    && substr($this->server('CONTENT_TYPE'), 0, 16) === 'application/json')
    {
      $this->post = json_decode(file_get_contents('php://input'), true);
    }

    $this->segments = explode('/', $this->path());
  }

  function segment($index=null, $default=null)
  {
    if (is_null($index)) return $this->segments;
    else return isset($this->segments[$index]) ? $this->segments[$index] : $default;
  }

  function port()
  {
    return (int) $this->server('server_port', 0);
  }

  # name and revision of the information protocol
  # via which the page was requested; i.e. 'HTTP/1.0';
  function protocol(string $default='HTTP/1.1'): string
  {
    return $this->server('server_protocol', $default);
  }

  # IP address from which request was made.
  function ip(): string
  {
    return $this->server('remote_addr');
  }

  # user's agent
  function agent(): string
  {
    return $this->server('http_user_agent');
  }

  # check if it is SSL connection
  function is_ssl(): bool
  {
    return in_array(strtolower($this->server('https')), ['on', '1'])
    || $this->server('server_port') === '443';
  }

  # current domain
  function host(): string
  {
    return $this->server('server_name');
  }


  function extract_path($path)
  {
    $path = explode('/', $path);
    $segs = [];
    foreach ($path as $seg)
    {
      if (!strpos($seg, '.')) $segs[] = $seg;
    }
    return implode($segs, '/');
  }

  function path()
  {
    static $path;

    if (!is_null($path)) return $path;

    if (isset($this->server['REQUEST_URI']))
    {
      $path = $this->server['REQUEST_URI'];
      log::info("Path, using `REQUEST_URI`: `{$path}`.", __CLASS__);
      $path = explode('?', $path)[0];
    }
    elseif (isset($this->server['PATH_INFO']) && $this->server['PATH_INFO'])
    {
      $path = trim($this->server['PATH_INFO'], '\\/');
      log::info("Path, using `PATH_INFO`: `{$path}`.", __CLASS__);
    }
    elseif (isset($this->server['PHP_SELF']))
    {
      $path = $this->server['PHP_SELF'];
      log::info("Path, using `PHP_SELF`: `{$path}`.", __CLASS__);
    }
    else
    {
      $path = '';
      log::warn("Path, could not find it, using empty.", __CLASS__);
    }

    if ($path === '/index.php') {
      $path = '';
    }

    $path = '/'.trim($path, '\\/');
    log::info("Path set to be: `{$path}`.", __CLASS__);
    return $path;
  }

  function get($id=null, $default=null)
  {
    if (is_null($id)) return $this->get;
    else return isset($this->get[$id]) ? $this->get[$id] : $default;
  }

  function post($id=null, $default=null)
  {
    if (is_null($id)) return $this->post;
    else return isset($this->post[$id]) ? $this->post[$id] : $default;
  }

  # get particular HTTP header (if sent)
  function header(string $id)
  {
    $headers = null;
    if (function_exists('getallheaders')) {
      $headers = getallheaders();
    }

    if ($headers && isset($headers[$id])) return $headers[$id];
    else if (isset($_SERVER['HTTP_'.strtoupper($id)])) return $_SERVER['HTTP_'.strtoupper($id)];
  }

  function method()
  {
    return strtoupper($this->server('REQUEST_METHOD'));
  }

  function server($key, $default=null)
  {
    $key = strtoupper($key);

    if (!array_key_exists($key, $this->server))
    {
      return $default;
    }
    else
    {
      return $this->server[$key];
    }
  }
}
