<?php

namespace http; class response
{
  private $data = [];
  private $headers = [
    'Content-Type' => 'text/html; charset=utf-8'
  ];
  private $status = 200;

  function set_status($status)
  {
    $this->status = $status;
  }

  function get_status()
  {
    return $this->status;
  }

  function set_header($key, $value=null)
  {
    if ($value) $this->headers[$key] = $value;
    else $this->headers[] = $key;
  }

  function get_header($key, $default=null)
  {
    if ($this->headers[$key]) return $this->headers[$key];
    else return $default;
  }

  function data($data, $id=null)
  {
    if ($id) $this->data[$id] = $data;
    else $this->data[] = $data;
    return true;
  }

  function json($data)
  {
    $this->set_header('Content-Type', 'application/json');
    $this->data(json_encode($data));
    return true;
  }

  function json_data($data) {
    return $this->json([ 'data' => $data ]);
  }

  function get_data($key=null, $default=null)
  {
    if (!$key) return $this->data;
    else if (isset($this->data[$key])) return $this->data[$key];
    else return $default;
  }

  function cors()
  {
    if (isset($_SERVER['HTTP_ORIGIN']))
    {
      header("Access-Control-Allow-Origin: *");
      header('Access-Control-Allow-Credentials: true');
      header('Access-Control-Max-Age: 86400');
    }

    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
    {
      http_response_code(200);
      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
          header("Access-Control-Allow-Methods: GET, POST, DELETE, PUT, OPTIONS");
      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
          header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
      exit(0);
    }
  }

  function flush()
  {
    # set the cors headers
    $this->cors();

    # set the code itself
    http_response_code($this->status);

    # apply headers
    foreach ($this->headers as $key => $header)
    {
      if (!is_numeric($key)) header("${key}: ${header}");
      else header($header);
    }

    # apply data
    print(implode("\n", $this->data));
  }
}
