<?php

namespace http; class route
{
  private $method;
  private $route;
  private $call;
  private $regex;
  private $segments;
  private $weight = 0;

  function __construct(string $method, string $route, callable $call)
  {
    $this->method = $method;
    $this->route = $route;
    $this->call = $call;

    list($this->segments, $this->regex) = $this->resolve(trim($route, '/'));
    $this->weight = count($this->segments);
  }

  function weight()
  {
    return $this->weight;
  }

  function call()
  {
    $r = req();
    $params = [];
    preg_match_all($this->regex, req()->path(), $segments, PREG_SET_ORDER);
    $segments = array_pop($segments);
    array_shift($segments);

    foreach ($this->segments as $segment)
    {
      if (!$segment['capture']) { continue; }
      if (!isset($segments[$segment['index']])) {
        if (!$segment['optional']) {
          err("No segment `{$segment['index']}` for: `{$this->regex}` on `".req()->path()."`");
        } else continue;
      }
      $match = $segments[$segment['index']];
      $match = trim($match, '/');
      if ($segment['multiple']) {
        $match = $match ? explode('/', $match) : [];
      }
      $params[] = $match;
    }
    return call_user_func_array($this->call, $params);
  }

  function match($method, $path)
  {
    return preg_match($this->regex, $path)
      && ($this->method === '*' || $this->method === $method);
  }

  private function resolve($route): array
  {
    $segments = explode('/', $route);
    $regex = '';
    $in_range = false;

    foreach($segments as $i => &$segment)
    {
      $segment = [
        'index'    => $i,
        'id'       => $segment,
        'path'     => $segment,
        'optional' => false,
        'capture'  => false,
        'multiple' => false,
        'regex'    => null
      ];

      if (substr($segment['path'], -1) === '?')
      {
        $segment['path'] = substr($segment['path'], 0, -1);
        $segment['optional'] = true;
      }

      if (substr($segment['path'], 0, 1) === ':')
      {
        $segment['id'] = substr($segment['path'], 1);
        $segment['regex'] = "(/[^/]*?/?)".($segment['optional']?'?':'');
        $segment['capture'] = true;
      }
      elseif (substr($segment['path'], 0, 3) === '...')
      {
        $segment['id'] = substr($segment['path'], 3);
        $segment['regex'] = "(/.*?)".($segment['optional']?'?':'');
        $segment['capture'] = true;
        $segment['multiple'] = true;
      }
      else
      {
        $segment['regex'] = "(/".preg_quote($segment['path'], '#').")".($segment['optional']?'?':'');
      }

      $regex = $regex.$segment['regex'];
    }

    $regex = "#^{$regex}$#i";
    return [$segments, $regex];
  }
}
