<?php

namespace http;

use \sys\event;

event::fire('before.init@http');

# Init base libraries for http
r('request',  new request($_SERVER, $_POST, $_GET));
r('response', new response());
r('router',   new router(r('request')));

event::on('after.init@sys', function() {
  r('router')->fire();
  r('response')->flush();
});

event::fire('after.init@http');
