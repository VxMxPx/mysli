<?php

/**
 * Return response object.
 * --
 * @param mixed $data
 * --
 * @return \sys\response
 */
function res($data=null)
{
  static $response;
  if (!$response) $response = r('response');
  if (is_null($data)) return $response;

  return is_string($data)
    ? $response->data($data)
    : $response->json($data);
}

/**
 * Return request object.
 * --
 * @return \sys\request
 */
function req()
{
  static $request;
  if (!$request) $request = r('request');
  return $request;
}

/**
 * Access _GET
 * --
 * @param string $id
 * @param mixed  $default
 * --
 * @return mixed
 */
function get($id=null, $default=null)
{
  return req()->get($id, $default);
}

/**
 * Access _POST
 * --
 * @param string $id
 * @param mixed  $default
 * --
 * @return mixed
 */
function post($id=null, $default=null)
{
  return req()->post($id, $default);
}
