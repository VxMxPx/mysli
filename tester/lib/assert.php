<?php

namespace tester; class assert
{

  /**
   * Assert that two values are equal by value and type.
   * --
   * @param mixed $actual
   * @param mixed $expect
   * --
   * @return array
   */
  static function equals($actual, $expect)
  {
    $r = [];

    $r['actual'] = static::describe($actual);
    $r['expect'] = static::describe($expect);
    $r['succeed'] = ($actual === $expect);

    return $r;
  }

  /**
   * Assert that value match a pattern.
   * --
   * @param  string $actual
   * @param  string $pattern
   * --
   * @return array
   */
  static function match($actual, $pattern)
  {
    $pattern = preg_quote($pattern);
    $pattern = str_replace('\\*', '.*?', $pattern);
    $pattern = "/{$pattern}/";

    $r = [];

    $r['actual'] = static::describe($actual);
    $r['expect'] = static::describe($pattern);

    $r['succeed'] = preg_match($pattern, $actual);

    return $r;
  }

  /**
   * Assert that value is an instance of.
   * --
   * @param  object $actual
   * @param  string $expect
   * --
   * @return array
   */
  static function instance($actual, $expect)
  {
    $r = [];

    $r['actual'] = static::describe($actual);
    $r['expect'] = [ 'instance', $expect ];

    $r['succeed'] = is_object($actual) && is_a($actual, $expect);

    return $r;
  }


  /**
   * Generate information about variable.
   * --
   * @param  mixed $variable
   * --
   * @return array
   */
  static function describe($var)
  {
    $type = strtolower( gettype($var) );

    if ($type !== 'object') return [$type, $var];

    # type is an object
    $class = get_class($var);
    return is_a($var, '\Exception')
      ? [
          'exception',
          $class,
          property_exists($var, 'id' ) ? $var->id() : '<undefined>',
          $var->getCode(),
          $var->getMessage()
        ]
      : ['instance', $class];
  }
}
