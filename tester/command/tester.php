<?php

namespace tester\command;

use \tester\{ test, diff };
use \sys\{ pkg, arr, str };
use \sys\fs\{ fs, dir, file, observer };
use \sys\cli\{ cla, output as out, util };

class tester
{
  /**
   * Run testing utility.
   * --
   * @param array $args
   * --
   * @return boolean
   */
  function run(array $args)
  {
    /*
    Set params.
     */
    $app = new cla('Mysli Testing Utility', __CLASS__);

    $app->set_help(true);

    $app
    ->create_parameter('PACKAGE', [
      'required' => true,
      'help'     => 'Package to be tested, in format: '.
                    '`package.class::method.filter`. '.
                    'Only `package` are required segments, '.
                    'use the rest to narrow down the amount of tests to be run.',
    ])
    ->create_parameter('--watch/-w', [
      'type' => 'boolean',
      'def'  => false,
      'help' => 'Watch package\'s directory and re-run tests when changes occurs.'
    ])
    ->create_parameter('--diff/-d', [
      'type' => 'boolean',
      'def'  => true,
      'help' => 'Print side-by-side comparison of expected/actual '.
                'results for failed tests.'
    ]);

    if (null !== ($r = cla::validate_and_print($app, $args))) { return $r; }

    list($package, $watch, $diff) = $app->get_values(
      'package', '--watch', '--diff'
    );

    return $watch
      ? $this->watch($package, $diff)
      : $this->test($package, $diff);
  }

  /**
   * Test particular package.
   * --
   * @param string  $pid In format: package.class::method.filter
   * @param boolean $diff
   * --
   * @return boolean
   */
  private function test($pid, $diff)
  {
    list($path, $package, $filter) = $this->ppf($pid);
    if (!$package)
    {
      out::error('ERROR', "No tests found for: `{$pid}`");
      return false;
    }

    // Get list of tests to run, by providing a package name.
    $testfiles = $this->get_tests_by_pid($pid);

    // If there's no tests available, just skip...
    if (empty($testfiles))
    {
      out::warning('WARNING', "No tests for: `{$package}`.");
      return false;
    }

    /*
    Some general meters...
     */
    $sum_succeeded = 0;
    $sum_skipped   = 0;
    $sum_failed    = 0;
    $sum_all       = 0;
    $sum_time      = 0;

    $last_dir = null;

    /*
    Loop through tests...
     */
    foreach ($testfiles as $testfile)
    {
      // If there's __init load once, it before file 0
      if ($last_dir !== dirname($testfile))
      {
        // Print class if necessary
        $class = substr(dirname($testfile), strlen($path));

        if ($class) { out::info("CLASS", $class); }

        $last_dir = dirname($testfile);
        $__init = dirname($testfile).'/__init.php';
        if (file_exists($__init))
          include($__init);
      }

      // Test base filename
      $testfilebase = substr(basename($testfile), 0, -4);

      // Get results a from file
      try
      {
        list($global, $tests, $tnamespace) = test::file($testfile);
      }
      catch (\Exception $e)
      {
        out::error("ERROR:\n".$e->getMessage());
        return false;
      }

      // Loop through results, and generate report.
      foreach ($tests as $testcase)
      {
        // Generate test...
        $test_generated = test::generate($testcase, $global, $tnamespace);

        // Run tests...
        $res = test::run($test_generated, $testcase, $global);

        // Update general stats
        $sum_succeeded += ($res['succeed'] === true);
        $sum_skipped   += ($res['skipped'] !== null);
        $sum_failed    += ($res['succeed'] === false);
        $sum_all++;
        $sum_time += $res['runtime'];

        // Succeed, failed, skipped?
        if ($res['succeed'])
        {
          out::format("<green>SUCCEED:</green> [{$testfilebase}] {$testcase['title']}\n");
        }
        elseif ($res['skipped'])
        {
          out::format("<yellow>SKIPPED:</yellow> [{$testfilebase}] {$testcase['title']}\n");
          out::format("         {$testcase['skip']}\n");
          out::nl();
        }
        else
        {
          out::format("<red>FAILED:</red> [{$testfilebase}] {$testcase['title']}\n");
          out::line("  FILE: {$testfile}");
          out::line("  LINE: {$testcase['lineof']['test']}");
          if ($diff)
          {
            out::nl();
            $this->generate_diff($res['expect'], $res['actual']);
          }
          out::nl();
        }
      }
    }

    // Generate nice footer with all stats.
    $this->generate_stats(
      $sum_succeeded, $sum_skipped, $sum_failed, $sum_all, $sum_time);
    out::nl();

    // If non failed, then this succeeded, otherwise, failed.
    return $sum_failed === 0;
  }

  /**
   * Run test(s) for particular package, and watch for change.
   * --
   * @param  string  $pid In format: package.class::method.filter
   * @param  boolean $diff    Weather to print diff for failed tests.
   * --
   * @return boolean
   */
  private function watch($pid, $diff)
  {
    list($path, $package, $filter) = $this->ppf($pid);

    // Check if package // Dir exists...
    if (!$package || !dir::exists($path))
      return false;

    // Loop will actually re-run this script multiple times (calling system),
    // so -w / --watch needs to be removed to avoid infinite loops...
    $arguments = $_SERVER['argv'];
    if (false !== ($k = array_search('-w', $arguments)))
      unset($arguments[$k]);
    if (false !== ($k = array_search('--watch', $arguments)))
      unset($arguments[$k]);

    // Setup observer
    $observer = new observer(pkgpath($package));
    $observer->set_filter("(*.php|{$filter})");
    $observer->set_interval(2);

    // Wait for changes
    $observer->observe(function ($changes) use ($pid, $diff, $arguments)
    {
      // Call self over and over again
      // This is done in such way, so that changes in PHP files are
      // registered. Each run is fresh...
      system(implode(" ", $arguments));
    });
  }

  /**
   * Get all tests using package's full ID/name.
   * --
   * @param string $pid package.class::method.filter
   * --
   * @return array
   *         [/full/absolute/test/path/file.phpt, ...]
   */
  private function get_tests_by_pid($pid)
  {
    list($path, $_, $filter) = $this->ppf($pid);

    // Is actual directory there...
    if (!dir::exists($path))
    {
      out::warning("Path not found: `{$path}`");
      return false;
    }

    // Find tests
    return file::find($path, $filter);
  }

  /**
   * Return path, package, filter.
   * --
   * @param  string $pid package.sub.class::method.filter
   * --
   * @return array
   *         [ string $path, string $package, string $filter ]
   */
  private function ppf($pid)
  {
    $pid = str::clean($pid, '<[^a-z0-9_\.\-\:\ \*]+>i');
    // Get Method/Filter
    $pids = explode('::', $pid, 2);
    $pidroot = $pids[0];
    if (isset($pids[1]))
    {
      $method_filter = explode('.', $pids[1]);
      $method = $method_filter[0];
      if (isset($method_filter[1]))
        $filter = $method_filter[1];
      else
        $filter = null;

      $filter = $method.($filter ? ".{$filter}" : '*').'.t.php';
    }
    else
    {
      $filter = "*.t.php";
    }

    # package
    $pkg = pkg::by_namespace(str_replace('.', '\\', $pidroot));
    $classes = substr($pidroot, strlen($pkg->id)+1);
    $classes = str_replace('.', '/', $classes);

    // Actual path to the tests...
    $path = pkgpath($pkg->id, 'test', $classes);

    return [$path, $pkg->id, $filter];
  }

  /**
   * Generate and print diff.
   * --
   * @param  array $expect
   * @param  array $actual
   * --
   * @return void
   */
  private function generate_diff(array $expect, array $actual)
  {
    $width = util::terminal_width();
    $width = $width < 50 ? $width : 50;

    // Expected
    out::format("<light_green> :: EXPECT</light_green>");
    out::format("<green>".str_repeat("^", $width+11)."</green>");
    out::nl();
    $this->output_diff( diff::plain($expect) );
    out::nl();

    // Actual
    out::format("<light_red> :: RESULT</light_red>");
    out::format("<red>".str_repeat("^", $width+11)."</red>");
    out::nl();
    $this->output_diff( diff::generate($actual, $expect) );
    out::nl();
  }

  /**
   * Output generated diff array.
   * --
   * @param array $diff
   */
  private function output_diff(array $diff)
  {
    foreach ($diff as list($is_diff, $level, $line1, $line2))
    {
      $space = str_repeat(' ', $level*4);

      if ($is_diff)
      {
        $space = substr_replace($space, '->', -3, 2);
        out::format("<red>".$space.$line1."</red>");
        out::nl();
      }
      else
      {
        out::line($space.$line1);
      }
    }
  }

  /**
   * Generate and print final footer status.
   * --
   * @param integer $succeed
   * @param integer $skipped
   * @param integer $failed
   * @param integer $all
   * @param float   $time
   * --
   * @return void
   */
  private function generate_stats(
    $succeed, $skipped, $failed, $all, $time)
  {
    $width = util::terminal_width();
    $width = $width < 60 ? $width : 60;

    // Set variables to be printed
    $all     = "RUN: {$all}";
    $failed  = $failed > 0  ? "<red>FAILED: {$failed}</red>"         : "FAILED: 0";
    $succeed = $succeed > 0 ? "<green>SUCCEED: {$succeed}</green>"   : "SUCCEED: 0";
    $skipped = $skipped > 0 ? "<yellow>SKIPPED: {$skipped}</yellow>" : "SKIPPED: 0";
    $time    = "RUN TIME: " . number_format($time, 4);

    out::line(str_repeat('-', $width));
    out::format("{$all} | {$failed} | {$succeed} | {$skipped} | {$time}\n");
  }
}
