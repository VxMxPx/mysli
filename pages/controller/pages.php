<?php namespace pages\controller;

use \sys\file;

class pages extends \sys\controller
{
  # resolve url path
  function resolve(array $params): string
  {
    $path = '';
    $segments = [];
    $site = r('site');

    # cut extension, this would allow
    # to properly route urls like: about/us.html => about/us
    $extension = '';
    if (!empty($params)) {
      $last = &$params[count($params)-1];
      if (\strpos($last, '.')) {
        $extension = file::extension($last);
        $last = \substr($last, 0, \strlen($extension)*-1);
      }
    }

    # if empty, assume index
    if (!count($params)) $params = ['index'];

    # allow loading parent page if child page is not matched
    while(count($params))
    {
      $path = implode('/', $params);
      if ($site->exists($path)) break;
      array_unshift($segments, array_pop($params));
    }

    return $this->serve($path, $extension, $segments);
  }

  # serve specific path (error if path not found, or not supported)
  function serve(string $path, string $extension='', array $segments=[])
  {
    $site = r('site');
    $page = $site->get($path);

    if (!$page) return $this->error(404);

    # is extension allowed?
    $allowed_extensions = explode(',', $page->extensions);
    if ($extension && $page->extensions)
    {
      if (!in_array($extension, $allowed_extensions)) {
        return $this->error(404);
      }
    }

    # redirect?
    if ($page->redirect && is_array($page->redirect))
    {
      if ($page->redirect['response_code']) {
        res()->set_status($page->redirect['response_code']);
      } else {
        res()->set_status(301);
      }
      res()->set_header('Location', $page->redirect['location']);
      return;
    }

    return $page->render();
  }

  function error(int $code=404)
  {
    $site = r('site');
    res()->set_status($code);
    return $site->exists('/error/'.$code)
      ? $site->get('/error/'.$code)->render()
      : t('pages.error.no_page');
  }
}
