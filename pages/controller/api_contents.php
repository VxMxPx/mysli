<?php namespace pages\controller;

class api_contents
{
  function get_contents()
  {
    $root = new \pages\entry('/');
    return $root->tree;
  }
}