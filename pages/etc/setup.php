<?php namespace pages\etc;

use \sys\fs\{file,dir};

define('PAGES_DIR_NAME', 'pages');

class setup
{
  static $intro_ini = <<<INTROINI
  ; this file was automatically generated
  ; feel free to change it
  ; generated @ {{DATETIME}}
  INTROINI;

  static $site_ini = <<<SITEINI
  title=Mysli Website

  [children]
  keywords=mysli cms website content management system index template editor php
  description=Mysli Powered WebApp.
  body=<<<page.md
  layout=layout.html,{theme}/layout.html
  media=media
  extensions=html,htm
  SITEINI;

  static $index_ini = <<<INDEXINI
  title="Hello World!"
  INDEXINI;
  static $index_md = <<<INDEXMD
  # Hello to Mysli Website

  Feel free to edit this file in `pages/index/page.ini`.
  INDEXMD;

  static $error_ini = <<<ERRORINI
  title="Error!"
  ERRORINI;
  static $error_md = <<<ERRORMD
  # Error!

  Sorry, and error occurred... Try to return to the [homepage](/).
  ERRORMD;

  static $theme_ini = <<<THEMEINI
  name=Default
  version=1
  authors=Mysli<hi@mysli.si>
  description=Defauly Mystli Theme.
  categories=simple,default
  THEMEINI;

  static $util_php = <<<'UTILPHP'
  <?php

  /**
   * Print the site navigation.
   */
  function navi($item)
  {
    $elements = ['<ul>'];
    foreach ($item->tree as $si) {
      $elements[] = '<li>';
      $elements[] = '<a href="'.$si->uri.'">'.$si->title.'</a>';
      $elements[] = navi($si);
      $elements[] = '</li>';
    }
    $elements[] = '</ul>';
    return implode("\n", $elements);
  }
  UTILPHP;

  static $layout_html = <<<'LAYOUTHTML'
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <title>{{page->title->append(' - ')}}{{site->title}}</title>
    <link rel="stylesheet" href="{{theme->asset('main.css')}}" />
    <script>if (top !== self) top.location = self.location;</script>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0" />
    <link rel="icon" type="image/png" href="{{theme->asset('favicon.png')}}" />
    <meta name="description" content="{{page->description}}">
    <meta name="keywords" content="{{page->keywords}}">
    {{page->style}}
    {{page->javascript}}
  </head>
  <body>
    <div id="app">
      {{navi($site->root())}}
      <article>
        {{yield}}
      </article>
    </div>
  </body>
  </html>
  LAYOUTHTML;

  static $main_css = <<<MAINCSS
  body {
    background: #ffffff;
    color: #191919;
    font-family: sans-serif;
  }
  #app {
    margin: 20px auto;
    max-width: 800px;
  }
  h1, h2, h3 {
    font-family: serif;
  }
  #app article img {
    border-radius: 4px;
    width: 100%;
  }
  MAINCSS;

  static function enable()
  {
    # directories to be created
    $dirs = [
      datpath('themes'),
      datpath('themes/default'),
      datpath('themes/default/autoload'),
      datpath('themes/default/layouts'),
      datpath('themes/default/assets'),
      datpath(\PAGES_DIR_NAME),
      pubpath(\PAGES_DIR_NAME),
    ];

    foreach($dirs as $dir)
      if (!is_dir($dir)) { mkdir($dir, 0777, true); }

    $files = [
      datpath('themes/default/theme.ini') => static::$theme_ini,
      datpath('themes/default/autoload/util.php') => static::$util_php,
      datpath('themes/default/layouts/layout.html') => static::$layout_html,
      datpath('themes/default/assets/main.css') => static::$main_css,
    ];

    foreach($files as $file_path => $file_contents) {
      file::write($file_path, $file_contents);
    }

    $intro_ini = str_replace('{{DATETIME}}', date('c'), static::$intro_ini)."\n";

    # install sample data if not there already
    # site.ini
    $site_ini_path = datpath('pages/site.ini');
    if (!file::exists($site_ini_path)) {
      file::write($site_ini_path, $intro_ini.static::$site_ini);
    }
    # index.md
    $index_md_path = datpath('pages/index');
    if (!dir::exists($index_md_path)) {
      dir::create($index_md_path);
      file::write(ds($index_md_path, 'page.ini'), $intro_ini.static::$index_ini);
      file::write(ds($index_md_path, 'page.md'), static::$index_md);
    }
    # error.md
    $error_md_path = datpath('pages/error');
    if (!dir::exists($error_md_path)) {
      dir::create($error_md_path);
      file::write(ds($error_md_path, 'page.ini'), $intro_ini.static::$error_ini);
      file::write(ds($error_md_path, 'page.md'), static::$error_md);
    }

    return true;
  }
}
