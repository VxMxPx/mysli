<?php namespace pages;

use \sys\fs\dir;

class media_collection
{
  function __construct(string $uri, string $media_dir)
  {
    $this->uri = $uri;
    $this->media_dir = $media_dir;

    $this->path = datpath(\PAGES_DIR_NAME, $uri, $media_dir);
    $this->pubpath = pubpath(\PAGES_DIR_NAME, $this->uri, $this->media_dir);
  }

  function exists()
  {
    return is_dir($this->path);
  }

  function is_published()
  {
    return is_dir($this->pubpath);
  }

  function publish_all()
  {
    return dir::copy($this->path, $this->pubpath);
  }
}