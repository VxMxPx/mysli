<?php namespace pages;

use \sys\{ini, ym, json};
use \markdown\{markdown, parser};


class processor
{
  static function markdown(string $markdown_source, string $uri): string
  {
    # markdown parser
    $parser = new parser($markdown_source);

    # add costume link handling to the parser for media files
    $link = $parser->get_processor('markdown.module.link');
    $link->set_local_url(
      "#^media/(.*?)(?<!\.html|\.php|\.htm)$#",
      ds(\PAGES_DIR_NAME, $uri));

    # process finally
    $processed = markdown::process($parser);

    # table of contents
    $headers = $parser->get_processor('markdown.module.header');
    $toc = $headers->as_array();

    # footnotes
    $footnote = $parser->get_processor('markdown.module.footnote');

    # maybe in future this could be used
    // $processed = [
    //   'toc'        => $toc,
    //   'references' => $footnote->as_array(),
    //   'html'       => $processed
    // ];

    return $processed;
  }

  static function process_file(string $filename, string $uri, string $type=null): string
  {
    if (!$type) $type = pathinfo($filename, PATHINFO_EXTENSION);

    switch ($type)
    {
      case 'ini':
        return ini::decode_file($filename);

      case 'ym':
        return ym::decode_file($filename);

      case 'json':
        return json::decode_file($filename);

      case 'md':
        return self::markdown(file_get_contents($filename), $uri);

      default:
        return file_get_contents($filename);
    }
  }
}