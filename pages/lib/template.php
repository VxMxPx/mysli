<?php namespace pages;

use \sys\{str, cache};

class theme_helper
{
  function __construct(string $name)
  {
    $this->name = $name;
  }

  function asset($file)
  {
    return "/themes/{$this->name}/assets/{$file}";
  }
}

class template
{
  function __construct(\pages\site $site, \pages\page $page)
  {
    $this->page = $page;
    $this->site = $site;
    $this->theme_name = cfg('pages.theme');
    $this->variables = ['site' => $site, 'page' => $page, 'theme' => new theme_helper($this->theme_name)];
    $this->theme_path = datpath('themes', $this->theme_name);
    $this->debug = cfg('pages.debug');

    if (!is_dir($this->theme_path))
      err('theme_not_found_in', $this->theme_path);
  }

  function eval(string $filename): string
  {
    extract($this->variables);

    ob_start();
    include $filename;
    $evaluated = ob_get_contents();
    ob_end_clean();

    return $evaluated;
  }

  function set_var($name, $value)
  {
    $this->variables[$name] = $value;
  }

  function process_layout(string $contents, string $path): string
  {
    # resolve {{extends layout set contents}}
    preg_replace_callback(
      '/{{extends ([a-z-0-9_]+) set ([a-z0-9_]+)}}/im',
      function($m) use ($path, &$contents) {
        $file = $m[1];
        $var = $m[2];
        $base = file_get_contents(ds($path, $file.'.html'));
        $contents = str_replace(
          '{{'.$var.'}}',
          str_replace($m[0], '', $contents),
          $base);
      }, $contents);

    # resolve:
    # {{import file}}
    # {{import file processor}}
    # {{import variable from file}}
    # {{import variable processor from file}}
    $contents = preg_replace_callback(
      '/{{import ([a-z-0-9_\.]+)( [a-z0-9_]+)?( from ([a-z-0-9_\.]+))?}}/im',
      function($m) use ($path)
      {
        if (isset($m[4]))
        {
          $file = $m[4];
          $varname = $m[1];
        }
        else
        {
          $file = $m[1];
          $varname = false;
        }

        # set processor if exists
        # processor is used to modify loaded contents
        $processor = null;
        if (isset($m[2])) $processor = trim($m[2]);

        $filepath = ds($path, $file);
        $extension = pathinfo($file, PATHINFO_EXTENSION);

        if (!$extension) {
          $contents = $this->process(file_get_contents($filepath), $path);
        } else {
          $contents = processor::process_file($filepath, $this->page->uri, $extension);
        }

        $has_processor = $processor && function_exists($processor);
        if ($has_processor) $contents = $processor($contents);

        if ($varname)
        {
          $contents = json_encode($contents);
          $contents = '<?php $'.$varname." = json_decode(<<<EOL\n{$contents}\nEOL\n, true); ?>";
        }

        return $contents;

      }, $contents);

    # replace {{for}} {{if}} {{else}}
    $contents = str_replace(
      ['{{for}}', '{{if}}', '{{else}}'],
      ['<?php endforeach; ?>', '<?php endif; ?>', '<?php else: ?>'],
      $contents);

    # replace {{if condition}} {{elseif condition}}
    $contents = preg_replace('/{{(if|elseif) (.*?)}}/m', '<?php $1 ($2): ?>', $contents);

    # replace {{for condition}}
    $contents = preg_replace('/{{for (.*?)}}/m', '<?php foreach ($1): ?>', $contents);

    # replace {{function()}}
    $contents = preg_replace('/{{([a-z][a-z_0-9]+)\((.*?)}}/mi', '<?php echo $1($2; ?>', $contents);

    # replace {{variable}} => ?php echo $variable; ?
    $contents = preg_replace('/{{([a-z][a-z_0-9]+.*?)}}/mi', '<?php echo \$$1; ?>', $contents);

    return $contents;
  }

  function get_path(string $name): string
  {
    $path = null;

    # resolve layout name to absolute path
    if (substr($name, 0, 8) === '{theme}/') {
      $path = ds($this->theme_path,'layouts', substr($name, 8));
    } else {
      $path = ds($this->page->path, $name);
    }

    return $path;
  }

  function render()
  {
    $page = $this->page;
    $cache = new cache($page->path);

    # maybe while page is cached..
    if ($cache->exists() && !$this->debug) {
      return $cache->get();
    }

    $layouts = str::split_trim($page->layout, ',');
    $layouts = array_reverse($layouts);
    $body = $page->body->processed();

    foreach($layouts as $layout)
    {
      $path = $this->get_path($layout);
      if (!file_exists($path)) continue;
      if (!$path) continue;
      $pk_cache = new cache($path);
      if ($pk_cache->exists() && !$this->debug) {
        $layout_content = $pk_cache->get();
      } else {
        $layout_content = file_get_contents($path);
        $layout_content = $this->process_layout($layout_content, $path);
        $pk_cache->set($layout_content);
      }
      $body = str_replace('<?php echo $yield; ?>', $body, $layout_content);
    }

    # place body to file, in order to eval
    $body_file = tmppath('cache', 'pages.template.eval.php');
    file_put_contents($body_file, $body);
    # finally get evaluated page
    $html = $this->eval($body_file);
    # cleanup
    unlink($body_file);
    # set cache
    $cache->set($html);
    return $html;
  }
}
