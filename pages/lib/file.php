<?php namespace pages;

class file
{
  function __construct(string $path, $parent)
  {
    $this->path = $path;
    $this->parent = $parent;
    $this->exists = file_exists($this->path);

    $this->raw = null;
    $this->processed = null;
  }

  function processed(): string
  {
    if (!$this->processed && $this->exists) {
      $this->processed = processor::process_file($this->path, $this->parent->uri);
    }
    return $this->processed;
  }

  function raw(): string
  {
    if (!$this->raw && $this->exists) {
      $this->raw = file_get_contents($this->path);
    }
    return $this->raw;
  }
}