<?php namespace pages;

use \sys\ini;

class site
{
  function __construct()
  {
    $this->path = datpath(\PAGES_DIR_NAME);

    # metadata
    $this->meta = ini::decode_file(ds($this->path, 'site.ini'));
    $this->meta['path'] = $this->path;
    $this->meta['type'] = 'site';
  }

  function exists(string $id): bool
  {
    return file_exists(ds($this->path, $id, 'page.ini'));
  }

  function get(string $id)
  {
    if ($this->exists($id)) {
      return new page($id);
    } else {
      return null;
    }
  }

  function root()
  {
    return new \pages\entry('/');
  }

  function __get($name)
  {
    # return metadata
    $value = isset($this->meta[$name]) ? $this->meta[$name] : '';
    return is_string($value) || is_int($value) ? new property($value) : $value;
  }
}
