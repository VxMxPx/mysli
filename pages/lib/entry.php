<?php namespace pages;

class entry
{
  function __construct(string $uri)
  {
    $this->uri = $uri;
    $this->path = datpath(\PAGES_DIR_NAME, $uri);

    $this->collections = [
      'parents' => ($uri && $uri !== 'index') ? $this->reload_parents() : [],
      'children' => '',
      'tree' => ''
    ];

    # metadata
    $this->meta = [];
    $this->meta['id'] = trim($this->uri, '/');
    $this->meta['uri'] = $this->uri;
    $this->meta['uid'] = str_replace('/', '-', $this->meta['id']);
    $this->meta['path'] = $this->path;
    $this->meta['title'] = basename($uri);
    $this->meta['type'] = 'entry';

    foreach ($this->collections['parents'] as $parent)
      if (isset($parent->meta['children']))
        $this->meta = array_merge($parent->meta['children'], $this->meta);
  }

  # re-read parent pages
  function reload_parents(): \pages\collection
  {
    $site = r('site');
    $segments = explode('/', $this->uri);
    $parents = new collection();

    while (count($segments) > 1)
    {
      array_pop($segments);
      $uri = implode('/', $segments);
      $parents->push($site->get($uri));
    }

    return $parents;
  }

  function reload_children(): \pages\collection
  {
    $dirs = scandir($this->path);
    $children = new collection();

    foreach ($dirs as $child)
    {
      if (substr($child, 0, 1) === '.') continue;
      $child = r('site')->get(ds($this->uri, $child));
      if ($child) $children->push($child);
    }

    return $children;
  }

  function reload_tree(): array
  {
    $tree = $this->children;
    foreach ($tree as $node) $node->tree;
    return (array) $tree;
  }

  function __get($name)
  {
    # has own property, which can be reloaded
    if (isset($this->collections[$name]))
    {
      if (!$this->collections[$name] && method_exists($this, 'reload_'.$name))
        $this->collections[$name] = call_user_func([$this, 'reload_'.$name]);
      return $this->collections[$name];
    }
    # return metadata
    $value = isset($this->meta[$name]) ? $this->meta[$name] : '';
    # this is a file input request, return a file pointer...
    if (is_string($value) && substr($value, 0, 3) === '<<<') {
      return new file(ds($this->path, trim(substr($value,3))), $this);
    }
    return is_string($value) || is_int($value) ? new property($value) : $value;
  }

}