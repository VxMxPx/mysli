<?php namespace pages;

class property
{
  function __construct($string)
  {
    $this->string = (string) $string;
  }

  function or($input): \pages\property
  {
    $input = is_subclass_of($input, '\\pages\\property')
      ? $input
      : new property($input);

    return $this->string ? $this->string : $input;
  }

  function prefix($string): \pages\property
  {
    return $this->string ? new property($string.$this->string) : $this;
  }

  function append($string): \pages\property
  {
    return $this->string ? new property($this->string.$string) : $this;
  }

  function __toString(): string
  {
    return $this->string;
  }

}
