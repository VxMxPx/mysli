<?php namespace pages;

class media_file
{
  function __construct(string $uri, string $media_dir, string $file)
  {
    $this->uri = $uri;
    $this->media_dir = $media_dir;
    $this->file = $file;

    $this->path = datpath(\PAGES_DIR_NAME, $uri, $media_dir, $file);
  }

  # get file url if file is published
  function url(string $url) {}

  function publish() {}

  function unpublish() {}

  function is_published() {}

  function resize($w, $h) {}

  function __toString() {    }
}
