<?php namespace pages;

class collection extends iterator
{

  # item's property does not have value
  # example: {page->children->not('is_media')}
  function not(string $field): \pages\collection
  {
    $arr = $this->getArrayCopy();
    return new collection(array_filter($arr, function($item) use ($field)
    {
      return !$item->{$field};
    }));
  }

  # sort items by a particular criteria
  # $field is the field by which to sort
  # $desc is sorting order
  # $convert (callable) a conversion can be performent on property
  #          example: strtotime, to sort by date
  function sort(string $field, bool $desc=false, $convert=null): \pages\collection
  {
    $arr = $this->getArrayCopy();
    $ord = $desc ? -1 : 1;
    if ($convert && !is_callable($convert)) err('convert_must_be_callable');
    usort($arr, function($a, $b) use ($field, $ord, $convert)
    {
      return $convert
        ? ($convert($a->{$field}) <=> $convert($b->{$field}))*$ord
        : ($a->{$field} <=> $b->{$field})*$ord;
    });
    return new collection($arr);
  }

  # sort items by a particular criteria, $field should be date
  function sort_date(string $field, bool $desc=false)
  {
    return $this->sort($field, $desc, 'strtotime');
  }

  # filter items by a particular field's value
  # $field is property name for a filter
  # $operation valid operations are:
  #   == equals
  #   != not equals
  #   ~ contains
  #   =* starts with $value
  #   *= ends with $value
  #   $value can be used in its place in that case default == will be used
  # $value possible values are:
  #   anything to check if value match directly
  #   /string/ to use regular expression
  #   [string, string] to use or operator
  #   function($value) to use function
  #   (note: $value can be used as a second argument instead of $operation)
  function filter(string $field, $operation, $value=null)
  {
    $arr = $this->getArrayCopy();

    if (!$value)
    {
      $value = $operation;
      $operation = '==';
    }

    return new collection(array_filter($arr, function($item) use ($field, $value)
    {
      switch($operation)
      {
        case '==':
          if (is_array($value))
          {
            $valid = false;
            foreach($value as $v) $v == $item->${field} ? $valid = true : null;
            return $valid;
          }
          elseif (is_callable($value, true))
          {
            return $value($item->${field});
          }
          elseif (substr($value, 0, 1) === '/')
          {
            return preg_match($value, $item->{$field});
          }
          else
          {
            return $item->{$field} == $value;
          }

        case '!=':
          return $item->{$field} != $value;

        case '~':
          return !!mb_stripos($item->{$field}, $value);

        case '*=':
          return substr($item->{$field}, -strlen($value)) === $value;

        case '=*':
          return substr($item, 0, strlen($value)) === $value;
      }
    }));
  }
}
