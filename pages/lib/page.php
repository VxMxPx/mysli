<?php namespace pages;

use \sys\ini;

class page extends entry
{
  function __construct(string $uri)
  {
    parent::__construct($uri);

    $this->meta_file = datpath(\PAGES_DIR_NAME, $uri, 'page.ini');

    if (!file_exists($this->meta_file)) err('no_meta_file_for_page', $this->uri);
    $this->meta = array_merge(r('site')->children, $this->meta, ini::decode_file($this->meta_file));
    $this->meta['type'] = 'page';

    $this->media_dir = isset($this->meta['media']) ? $this->meta['media'] : 'media';

    $style_file = datpath(\PAGES_DIR_NAME, $uri, 'page.css');
    if (file_exists($style_file)) $this->meta['style'] = '<style>'.file_get_contents($style_file).'</style>';

    $js_file = datpath(\PAGES_DIR_NAME, $uri, 'page.js');
    if (file_exists($js_file)) $this->meta['javascript'] = '<script>'.file_get_contents($js_file).'</script>';
  }

  # get media file/collection
  function media(string $file='')
  {
    return $file
      ? new media_file($this->uri, $this->media_dir, $file)
      : new media_collection($this->uri, $this->media_dir);
  }

  function render()
  {
    # check for media directory and publish if needed
    $media = $this->media();
    if ($media->exists()) {
      if (!$media->is_published() || cfg('pages.media_force')) {
        $media->publish_all();
      }
    }

    $template = new template(r('site'), $this);
    return $template->render();
  }
}