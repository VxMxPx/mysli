<?php namespace pages;

define('PAGES_DIR_NAME', 'pages');

use \sys\fs\dir;

# register site
r('site', new site());

# auto-load theme libraries
call_user_func(function() {
  $theme = cfg('pages.theme');
  $autoload_dir = datpath('themes', $theme, 'autoload');
  $files = scandir($autoload_dir);
  foreach($files as $file)
  {
    if (substr($file, -4) === '.php') {
      $filepath = ds($autoload_dir, $file);
      include $filepath;
    }
  }
});

# auto-publish theme if in debug mode
call_user_func(function() {
  $theme = cfg('pages.theme');
  $debug = cfg('pages.debug');
  $theme_pub = pubpath('themes', $theme, 'assets');
  $theme_dat = datpath('themes', $theme, 'assets');
  if (dir::exists($theme_dat) && (!dir::exists($theme_pub) || $debug)) {
    dir::copy($theme_dat, $theme_pub);
  }
});

# run route
$router = r('router');

$router->any('/...params', function($params=[]) {
  $pages = new controller\pages();
  return res($pages->resolve($params));
});
